<?php

/*
|--------------------------------------------------------------------------
| Virtual Portal routes
|--------------------------------------------------------------------------
|
| These routes take over as soon as the url turns into this
| 
| 	
| 
*/

Route::group( ['prefix' => '{project}/virtual', 'as' => 'virtual.', 'middleware' => ['web']], function(){

	Route::group( ['prefix' => '/admin', 'as' => 'admin.', 'middleware' => ['auth','client','project','virtual']], function(){
		Route::get( '/',									['as' => 'index',			'uses' => 'VirtualPortalController@index'] );
		Route::get('/settings',								['as' => 'settings',        'uses' => 'VirtualPortalController@settings'] );
		Route::get( '/reports',								['as' => 'reports',         'uses' => 'Front\VideoViewController@reports'] );
		Route::post( '/reports/data',						['as' => 'reports.data',    'uses' => 'Front\VideoViewController@data'] );

		Route::group( ['prefix' => '/zoom', 'as' => 'zoom.'], function(){
			Route::get( '/',								['as' => 'index',			'uses' => 'ZoomController@index'] );
			Route::get( '/list',							['as' => 'list',            'uses' => 'ZoomController@list'] );
			Route::get( '/settings/{meeting?}',				['as' => 'settings',		'uses' => 'ZoomController@settings'] );
			Route::post( '/create',							['as' => 'create',			'uses' => 'ZoomController@create'] );
			Route::post( '/update/{meeting?}',				['as' => 'update',          'uses' => 'ZoomController@update'] );
		} );

		Route::group( ['prefix' => '/users', 'as' => 'users.'], function(){
			Route::get( '/',								['as' => 'index',			'uses' => 'UsersController@index'] );
			Route::get( '/edit/{user}',						['as' => 'edit',            'uses' => 'UsersController@edit'] );
			Route::post( '/save/{user}',					['as' => 'save',            'uses' => 'UsersController@save'] );
			Route::get( '/speaker/{user?}',					['as' => 'speaker',			'uses' => 'UsersController@speaker'] );
			Route::post( '/speaker/{user?}/save_speaker',	['as' => 'save_speaker',	'uses' => 'UsersController@save_speaker'] );
			Route::get( '/login/{user?}',					['as' => 'login',           'uses' => 'UsersController@login'] );
		} );

		Route::group( ['prefix' => '/videos', 'as' => 'videos.'], function(){
			// Route::get( '/',								['as' => 'index',			'uses' => 'VideosController@index'] );
			Route::get( '/upload',							['as' => 'upload',          'uses' => 'VideosController@upload'] );
			Route::post( '/upload',							['as' => 'upload',          'uses' => 'VideosController@store'] );
			// Route::get( '/setup/{video?}',					['as' => 'setup',			'uses' => 'VideosController@setup'] );
			// Route::post( '/delete/{video}',					['as' => 'delete',			'uses' => 'VideosController@delete'] );
			// Route::get( '/link/',							['as' => 'link',			'uses' => 'VideosController@link'] );
			// Route::post( '/save/{video?}',					['as' => 'save',           	'uses' => 'VideosController@save'] );
		} );

	} );

	Route::group( ['as' => 'front.', 'middleware' => ['language']], function(){

		Route::get( '/login',								['as' => 'login',			'uses' => 'Front\VirtualPortalController@home'] );
		Route::get('loginAs/{regId}',						['as' => 'loginAs',			'uses' => 'Front\VirtualPortalController@loginAs']);
		Route::post( '/login',								['as' => 'login',			'uses' => 'Front\VirtualPortalController@login'] );
		Route::get( '/logout',								['as' => 'logout',			'uses' => 'Front\VirtualPortalController@logout'] );

		Route::group( ['middleware' => ['virtualfrontauth']], function(){
			Route::get( '/dashboard',						['as' => 'dashboard',		'uses' => 'Front\VirtualPortalController@dashboard'] );
			Route::get( '/portal', 							['as' => 'portal', 			'uses' => 'Front\VirtualPortalController@portal'] );
			Route::get( '/bio/{person}',					['as' => 'bio', 			'uses' => 'Front\VirtualPortalController@bio'] );
			Route::get( '/video_console/{activity}',		['as' => 'video_console', 	'uses' => 'Front\VirtualPortalController@video_console'] );
			Route::post( '/update_log/{video?}',			['as' => 'update_log',      'uses' => 'Front\VideoViewController@update'] );
			Route::get( '/agenda', 							['as' => 'agenda', 			'uses' => 'Front\VirtualPortalController@agenda'] );
			Route::get( '/schedule', 						['as' => 'schedule', 		'uses' => 'Front\VirtualPortalController@schedule'] );
			Route::get( '/schedule_frame', 					['as' => 'schedule_frame', 	'uses' => 'Front\VirtualPortalController@schedule_frame'] );
			Route::get( '/eposter', 						['as' => 'eposter', 		'uses' => 'Front\VirtualPortalController@eposter'] );
			Route::get( '/evaluation', 						['as' => 'evaluation', 		'uses' => 'Front\VirtualPortalController@evaluation'] );
			
			// Nisarg Edit : Currently calling the view directly - You can later setup the controller to do the logic and call this view
			// Route::get('/speakers_dashboard',function(\App\Project $project){
			// 	return view('virtual::public.speakers_dashboard')->with('project',$project);
			// })->name('speakers_dashboard');

			//Attendee list page
			Route::get('/attendee_list',function(\App\Project $project){
				return view('virtual::public.attendee_list')->with('project',$project);
			})->name('attendee_list');
			//Message center page
			Route::get('/message_center',function(\App\Project $project){
				return view('virtual::public.message_center')->with('project',$project);
			})->name('message_center');
			// Nisarg Edit ends

			Route::post( '/update_profile_picture',			['as' => 'update_profile_picture',	'uses' => 'Front\VirtualPortalController@update_profile_picture'] );
			Route::post( '/update',							['as' => 'update',			 		'uses' => 'Front\VirtualPortalController@update'] );

			Route::group( ['prefix' => '/session/{session?}', 'as' => 'session.'], function(){
				Route::get( '/',							['as' => 'index',           'uses' => 'Front\SessionController@index'] );
				Route::post( '/update',						['as' => 'update',          'uses' => 'Front\SessionController@update'] );
			} );

			Route::group( ['prefix' => '/speakers', 'as' => 'speaker.'], function(){

				Route::get('/me',							['as' => 'show',			'uses' => 'Front\SpeakerController@show']);
				Route::get('/me/edit',						['as' => 'edit',			'uses' => 'Front\SpeakerController@edit']);
				Route::post('/me/save',						['as' => 'save',			'uses' => 'Front\SpeakerController@save']);

				// Route::get( '/',							['as' => 'index',          	'uses' => 'Front\SpeakerController@index'] );
				// Route::post( '/upload',						['as' => 'upload',          'uses' => 'Front\SpeakerController@upload'] );
				// Route::post( '/check',						['as' => 'check',          	'uses' => 'Front\SpeakerController@check'] );
				// Route::get( '/setup/{video}',				['as' => 'setup',			'uses' => 'Front\SpeakerController@setup'] );
				// Route::post( '/save_video/{video}',			['as' => 'save_video',		'uses' => 'Front\SpeakerController@save_video'] );
			} );

			Route::group( ['prefix' => '/notes', 'as' => 'notes.'], function(){
				Route::get( '/get/{note?}',					['as' => 'get',          	'uses' => 'Front\NotesController@get'] );
				Route::post( '/save',						['as' => 'save',         	'uses' => 'Front\NotesController@save'] );
			} );

			Route::group( ['prefix' => '/discussion', 'as' => 'discussion.'], function(){
				Route::get( '/load',						['as' => 'load',			'uses' => 'Front\DiscussionController@load'] );
				Route::post( '/post',						['as' => 'post',			'uses' => 'Front\DiscussionController@post'] );
				Route::post( '/delete',						['as' => 'delete',			'uses' => 'Front\DiscussionController@delete'] );
				Route::post( '/like',						['as' => 'like',			'uses' => 'Front\DiscussionController@like'] );
			} );

			Route::group( ['prefix' => '/zoom', 'as' => 'zoom.'], function(){
				Route::get( '/',							['as' => 'join',            'uses' => 'Front\ZoomController@join'] );
				Route::get( '/connect',						['as' => 'connect',			'uses' => 'Front\ZoomController@connect'] );
				Route::get( '/meeting',						['as' => 'meeting',			'uses' => 'Front\ZoomController@meeting'] );
				Route::get( '/config/{meeting?}',			['as' => 'config',			'uses' => 'Front\ZoomController@config'] );
				Route::post( '/signature/{meeting?}',		['as' => 'signature',		'uses' => 'Front\ZoomController@signature'] );
			} );
		} );
	} );
} );
