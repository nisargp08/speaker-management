@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
<style>
.speakers_dashboard .color-1 {
	 color: #00bbd5;
}
 .speakers_dashboard .color-2 {
	 color: #fd5722;
}
 .speakers_dashboard .color-3 {
	 color: #009688;
}
 .speakers_dashboard .color-4 {
	 color: #e91d62;
}
 .speakers_dashboard .font-weight-600 {
	 font-weight: 600;
}
 .speakers_dashboard .f-12 {
	 font-size: 12px;
}
 .speakers_dashboard .f-14 {
	 font-size: 14px;
}
 .speakers_dashboard .green-text {
	 color: #28a745;
}
 .speakers_dashboard .red-text {
	 color: #dc3545;
}
 .speakers_dashboard .yellow-text {
	 color: #fff3cd;
}
 .speakers_dashboard .rounded-5px {
	 border-radius: 5px;
}
 .speakers_dashboard .title {
	 font-weight: 600;
	 padding: 1.5rem 0 0.5rem 0;
}
 .speakers_dashboard .vr {
	 height: 5px;
	 border-left: 2px solid grey;
	 margin-left: 1rem;
	 margin-right: 1rem;
}
 .speakers_dashboard .s_card {
	 background-color: white;
	 border-radius: 12px;
	 box-shadow: 0 4px 32px rgba(0, 0, 0, 0.12);
	 padding: 22px;
}
 .speakers_dashboard .s_card .card_header {
	 font-size: 24px;
	 line-height: 29px;
	 color: #4f4f4f;
	 font-weight: 800;
}
 .speakers_dashboard .speakers_info_card {
	 height: 100%;
}
 .speakers_dashboard .speakers_info_card .img_wrapper {
	 text-align: center;
}
 .speakers_dashboard .speakers_info_card .img_wrapper img {
	 border-radius: 12px;
	 max-width: 100%;
	 height: auto;
}
 .speakers_dashboard .speakers_info_card .speaker_name {
	 font-size: 24px;
	 line-height: 29px;
	 color: #4f4f4f;
	 font-weight: 600;
	 padding-top: 24px;
}
 .speakers_dashboard .speakers_stats_card {
	 height: 100%;
}
 .speakers_dashboard .speakers_stats_card .row {
	 padding: 11px;
}
 .speakers_dashboard .speakers_stats_card .stat_card {
	 display: flex;
	 padding: 16px;
	 margin-bottom: 10px;
	 align-items: center;
	 justify-content: space-between;
	 border-radius: 6px;
	 background-color: whitesmoke;
}
 .speakers_dashboard .speakers_stats_card .stat_card p {
	 margin: 0;
}
 @media (max-width: 992px) {
	 .speakers_dashboard .speakers_stats_card {
		 margin: 10px 0;
	}
}
 .speakers_dashboard .speakers_activity_card {
	 margin-top: 10px;
}
 @media (max-width: 992px) {
	 .speakers_dashboard .speakers_activity_card {
		 margin-top: 20px;
	}
}
 .speakers_dashboard .speakers_activity_card ul li {
	 display: flex;
	 flex-wrap: wrap;
	 align-items: center;
	 justify-content: space-between;
}
 .speakers_dashboard .speakers_activity_card .session_operations .dropdown-menu .dropdown-item {
	 transition: all 0.3s ease;
	 cursor: pointer;
}
 .speakers_dashboard .speakers_activity_card .session_operations .dropdown-menu .dropdown-item:focus, .speakers_dashboard .speakers_activity_card .session_operations .dropdown-menu .dropdown-item:active {
	 background-color: #f8f9fa;
}
 .speakers_dashboard .speakers_activity_card .q-answered, .speakers_dashboard .speakers_activity_card .q-pending {
	 padding: 5px 10px;
	 margin-left: 5px;
	 background-color: seagreen;
	 color: white;
	 border-radius: 5px;
}
 .speakers_dashboard .speakers_activity_card .q-pending {
	 background-color: #ffc107;
}
 </style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )
	<h2>Event Title</h2>
@endsection

@section( 'content' )
    <div class="container speakers_dashboard">
        <h2 class="title">Speakers Dashboard</h2>
        <hr>
        <div class="speakers_top">
            <div class="row">
                {{-- Speakers Info Card - Includes Name,photo and bio --}}
                <div class="col-lg-4">
                    <div class="speakers_info_card s_card">
                        <div class="img_wrapper">
                            {{-- Dynamic : Speaker profile photo --}}
                            <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="Speaker">
                        </div>
                        {{-- Dynamic : Speaker name --}}
                        <p class="speaker_name">John Wayne</p>
                        <p class="speaker_bio">
                            {{-- Dynamic : Speaker Bio - Trimming the characters to 255 and rest can be displayed in a modal --}}
                            {{str_limit('Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate ea natus adipisci deserunt doloribus. Minima ut deserunt accusamus quis officiis.',255) }}
                        </p>
                        <button class="btn btn-success" data-toggle="modal" data-target="#editBio"><i class="fas fa-pen"></i> <span class="pl-1">Edit</span></button>
                    </div>
                    <!-- Speaker Bio edit modal -->
                    <div class="modal fade" id="editBio" tabindex="-1" role="dialog" aria-labelledby="editBioLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title">Edit Speaker Information</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                {{-- Dynamic : Speaker's info here - Hard coding input values for demo purposes --}}
                                <label class="label font-weight-600 mt-2" for="name">Name</label>
                                <input type="text" id="name" class="form-control" value="John Wayne">
                                <label class="label font-weight-600 mt-2" for="bio">Bio</label>
                                <textarea class="form-control" id="bio" cols="10" rows="5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni impedit id perferendis aperiam labore dolorem vel esse sunt? Tenetur, ad?</textarea>
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-success">Update</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                {{-- Stas for Speaker --}}
                <div class="col-lg-8">
                    <div class="speakers_stats_card s_card">
                        <p class="card_header">Statistics</p>
                        <hr>
                        {{-- Stat card container --}}
                        <div class="row">
                            {{-- Looping for demo purposes --}}
                            @for($i = 0 ; $i < 3 ; $i++)
                                <div class="col-sm-6 col-md-4">
                                    <div class="stat_card">
                                        <p class="color-1"><i class="fas fa-cloud fa-2x "></i></p>
                                        <div class="text-right">
                                            <h4 class="m-0">9</h4>
                                            <p>Sessions</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="stat_card">
                                        <p class="color-2"><i class="fas fa-users fa-2x "></i></p>
                                        <div class="text-right">
                                            <h4 class="m-0">9</h4>
                                            <p>Viewed</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="stat_card">
                                        <p class="color-3"><i class="far fa-heart fa-2x "></i></p>
                                        <div class="text-right">
                                            <h4 class="m-0">19</h4>
                                            <p>Likes</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="stat_card">
                                        <p class="color-4"><i class="far fa-comments fa-2x "></i></p>
                                        <div class="text-right">
                                            <h4 class="m-0">25</h4>
                                            <p>Q/A</p>
                                        </div>
                                    </div>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="speakers_activity_card s_card">
                        <nav>
                            {{-- Activity Tabs --}}
                            <div class="nav nav-tabs" id="activity_tab" role="tablist">
                              <a class="nav-item nav-link active" id="nav-session-tab" data-toggle="tab" href="#nav-session" role="tab" aria-controls="nav-session" aria-selected="true">Sessions</a>
                              <a class="nav-item nav-link" id="nav-qa-tab" data-toggle="tab" href="#nav-qa" role="tab" aria-controls="nav-qa" aria-selected="false">Questions & Answers</a>
                            </div>
                          </nav>
                          {{-- Activity Tabs Content --}}
                          <div class="tab-content" id="activity_tabContent">
                            <div class="tab-pane fade show active" id="nav-session" role="tabpanel" aria-labelledby="nav-session-tab">
                                <div class="my-4 session_content">
                                    {{-- Message when speaker has no session --}}
                                    <div class="alert alert-info">
                                        <p class="m-0">When no session : You have not created any session</p>
                                    </div>
                                    {{-- Session display list --}}
                                    <ul class="list-group">
                                        {{-- Looping for demo purposes --}}
                                        {{-- Dynamic : Session Details --}}
                                        @for($i = 0 ; $i < 5 ; $i++)
                                            <li class="list-group-item">
                                                <div class="session_info">
                                                    <p class="font-weight-600 m-0 p-0">Imaginary Session {{$i+1}}</p>
                                                    <span class="date f-14">
                                                        <i class="fas fa-calendar-alt text-muted"></i> September 16,2020
                                                    </span>
                                                    <span class="time p-2 f-14">
                                                        <i class="fas fa-clock text-muted"></i> 05:03:52 PM - 12:00:00 AM
                                                    </span>
                                                </div>
                                                <div class="session_operations">
                                                    {{-- Session operation buttons - Edit session,video,slide deck,resources --}}
                                                    <div class="dropdown">
                                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" id="sessionSettings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          <i class="fas fa-cogs"></i><span class="pl-2">Settings</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right f-14" aria-labelledby="sessionSettings">
                                                            <a class="dropdown-item" title="Edit session details" data-toggle="modal" data-target="#sessionModal"><i class="fas fa-pen"></i><span class="pl-2">Session Details</span></a>
                                                            <a class="dropdown-item" title="Edit session Video details" data-toggle="modal" data-target="#videoModal"><i class="fas fa-file-video"></i><span class="pl-2">Session Video</span></a>
                                                            <a class="dropdown-item" title="Edit slide deck details" data-toggle="modal" data-target="#slideDeckModal"><i class="fas fa-film"></i><span class="pl-2">Slide Deck</span></a>
                                                            <a class="dropdown-item" title="Edit resources details" data-toggle="modal" data-target="#resourcesModal"><i class="fas fa-file-archive"></i><span class="pl-2">Resource List</span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endfor
                                    </ul>
                                </div>
                                <!-- Session Edit Modal -->
                                <div class="modal fade" id="sessionModal" tabindex="-1" role="dialog" aria-labelledby="sessionModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title">Edit Session</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            {{-- Dynamic : Session's info here --}}
                                            {{-- Two tabs - English and French --}}
                                            <div class="nav nav-tabs" id="session_tab" role="tablist">
                                                <a class="nav-item nav-link active" id="session-en-tab" data-toggle="tab" href="#session-en" role="tab" aria-controls="session-en" aria-selected="true">English</a>
                                                <a class="nav-item nav-link" id="session-fr-tab" data-toggle="tab" href="#session-fr" role="tab" aria-controls="session-fr" aria-selected="false">French</a>
                                            </div>  
                                            <div class="tab-content" id="session_tabContent">
                                                {{-- English session tab content --}}
                                                <div class="tab-pane fade show active my-4" id="session-en" role="tabpanel" aria-labelledby="session-en-tab">
                                                    <label class="label font-weight-600 my-2 d-block" for="sessionName">Session Name</label>
                                                    <input type="text" id="sessionName" class="form-control">

                                                    <label class="label font-weight-600 my-2 d-block" for="description">Session Description</label>
                                                    <textarea class="form-control" id="description" cols="10" rows="5"></textarea>
                                                </div>
                                                {{-- French session tab content --}}
                                                <div class="tab-pane fade my-4" id="session-fr" role="tabpanel" aria-labelledby="session-fr-tab">
                                                    <label class="label font-weight-600 my-2 d-block" for="sessionName">Session Name</label>
                                                    <input type="text" id="sessionName" class="form-control">

                                                    <label class="label font-weight-600 my-2 d-block" for="description">Session Description</label>
                                                    <textarea class="form-control" id="description" cols="10" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success">Update Session</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Video Edit Modal -->
                                <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title">Edit Video</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            {{-- Dynamic : Video's info here --}}
                                            {{-- Two tabs - English and French --}}
                                            <div class="nav nav-tabs" id="video_tab" role="tablist">
                                                <a class="nav-item nav-link active" id="video-en-tab" data-toggle="tab" href="#video-en" role="tab" aria-controls="video-en" aria-selected="true">English</a>
                                                <a class="nav-item nav-link" id="video-fr-tab" data-toggle="tab" href="#video-fr" role="tab" aria-controls="video-fr" aria-selected="false">French</a>
                                            </div>  
                                            <div class="tab-content" id="video_tabContent">
                                                {{-- English video tab content --}}
                                                <div class="tab-pane fade show active my-4" id="video-en" role="tabpanel" aria-labelledby="video-en-tab">
                                                    {{-- Video File dropdown --}}
                                                    <div class="form-row my-2 d-flex align-items-center">
                                                        <div class="col-md-12">
                                                            <label class="label font-weight-600" for="en-file">Video File</label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="file" name="en-file" id="en-file">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-row my-2 d-flex align-items-center">
                                                        <div class="col-md-4">
                                                            <label class="label font-weight-600" for="en-file-url">Video File</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <small>If the video is somewhere else, enter its location here.</small>
                                                            <input type="text" id="en-file-url" class="form-control">
                                                        </div>
                                                    </div>
                                                    <p class="red-text">Other options if allowed</p>
                                                </div>
                                                {{-- French video tab content --}}
                                                <div class="tab-pane fade my-4" id="video-fr" role="tabpanel" aria-labelledby="video-fr-tab">
                                                    {{-- Video File dropdown --}}
                                                    <div class="form-row my-2 d-flex align-items-center">
                                                        <div class="col-md-12">
                                                            <label class="label font-weight-600" for="fr-file">Video File</label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="file" name="fr-file" id="fr-file">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-row my-2 d-flex align-items-center">
                                                        <div class="col-md-4">
                                                            <label class="label font-weight-600" for="fr-file-url">Video File</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <small>If the video is somewhere else, enter its location here.</small>
                                                            <input type="text" id="fr-file-url" class="form-control">
                                                        </div>
                                                    </div>
                                                    <p class="red-text">Other options if allowed</p>
                                                </div>
                                            </div>                                    
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-success">Update Session</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Slide deck Edit Modal -->
                                <div class="modal fade" id="slideDeckModal" tabindex="-1" role="dialog" aria-labelledby="slideDeckModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title">Slide Deck</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            {{-- Two tabs - English and French --}}
                                            <div class="nav nav-tabs" id="slide_tab" role="tablist">
                                                <a class="nav-item nav-link active" id="slide-en-tab" data-toggle="tab" href="#slide-en" role="tab" aria-controls="slide-en" aria-selected="true">English</a>
                                                <a class="nav-item nav-link" id="slide-fr-tab" data-toggle="tab" href="#slide-fr" role="tab" aria-controls="slide-fr" aria-selected="false">French</a>
                                            </div>  
                                            <div class="tab-content" id="slide_tabContent">
                                                {{-- English slide tab content --}}
                                                <div class="tab-pane fade show active my-4" id="slide-en" role="tabpanel" aria-labelledby="slide-en-tab">
                                                    <div class="my-4 slide-content">
                                                        {{-- Message when no slidedeck presents --}}
                                                        <div class="alert alert-info">
                                                            <p class="m-0">When no Slidedeck : No Slides have been uploaded yet!</p>
                                                        </div>
                                                        {{-- Slide deck upload area - Embed Dropzone file upload here--}}
                                                        <form action="#" class="dropzone" id="">
                                                            <input type="file" name="file" />
                                                            <div class="my-2">
                                                                <button class="btn btn-success btn-sm">Upload</button>
                                                            </div>
                                                        </form>
                                                        <hr>
                                                        {{-- Slide preview --}}
                                                        <div>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <p class="m-0 inline text-muted f-12">To update the slide simply delete the existing slide and upload a new one</p>
                                                                <button class="btn btn-danger btn-sm my-2 text-right">Delete</button>
                                                            </div>
                                                            <iframe src="https://www.desystems.com/wp-content/uploads/2020/09/LogitechNextNormalDES.pdf" width="100%" height="500px"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- French slide tab content --}}
                                                <div class="tab-pane fade my-4" id="slide-fr" role="tabpanel" aria-labelledby="slide-fr-tab">
                                                    <div class="my-4 slide-content">
                                                        {{-- Message when no slidedeck presents --}}
                                                        <div class="alert alert-info">
                                                            <p class="m-0">When no Slidedeck : No Slides have been uploaded yet!</p>
                                                        </div>
                                                        {{-- Slide deck upload area - Embed Dropzone file upload here--}}
                                                        <form action="#" class="dropzone" id="">
                                                            <input type="file" name="file" />
                                                            <div class="my-2">
                                                                <button class="btn btn-success btn-sm">Upload</button>
                                                            </div>
                                                        </form>
                                                        <hr>
                                                        {{-- Slide preview --}}
                                                        <div>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <p class="m-0 inline text-muted f-12">To update the slide simply delete the existing slide and upload a new one</p>
                                                                <button class="btn btn-danger btn-sm my-2 text-right">Delete</button>
                                                            </div>
                                                            <iframe src="https://www.desystems.com/wp-content/uploads/2020/09/LogitechNextNormalDES.pdf" width="100%" height="500px"></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Resources Edit Modal -->
                                <div class="modal fade" id="resourcesModal" tabindex="-1" role="dialog" aria-labelledby="resourcesModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                                    <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title">Resources List</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            {{-- Two tabs - English and French --}}
                                            <div class="nav nav-tabs" id="resources_tab" role="tablist">
                                                <a class="nav-item nav-link active" id="resources-en-tab" data-toggle="tab" href="#resources-en" role="tab" aria-controls="resources-en" aria-selected="true">English</a>
                                                <a class="nav-item nav-link" id="resources-fr-tab" data-toggle="tab" href="#resources-fr" role="tab" aria-controls="resources-fr" aria-selected="false">French</a>
                                            </div>  
                                            <div class="tab-content" id="resources_tabContent">
                                                {{-- English resources tab content --}}
                                                <div class="tab-pane fade show active my-4" id="resources-en" role="tabpanel" aria-labelledby="resources-en-tab">
                                                    <div class="my-4 resources-content">
                                                        {{-- Message when no resources presents --}}
                                                        <div class="alert alert-info">
                                                            <p class="m-0">When no Resources : No Resources have been uploaded yet!</p>
                                                        </div>
                                                        {{-- Slide deck upload area - Embed Dropzone file upload here--}}
                                                        <form action="#" class="dropzone" id="">
                                                            <input type="file" name="file" />
                                                            <div class="my-2">
                                                                <button class="btn btn-success btn-sm">Upload</button>
                                                            </div>
                                                        </form>
                                                        {{-- Resources list --}}
                                                        <div>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Uploaded on</th>
                                                                            <th>Operations</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Test.pdf</td>
                                                                            <td>2020-08-06</td>
                                                                            <td>
                                                                                <a href="#">
                                                                                    <i class="fas fa-eye fa-lg pr-2"></i>View
                                                                                  </a>
                                                                                  <span class="vr"></span>
                                                                                  <a href="#" class="red-text">
                                                                                    <i class="fas fa-trash fa-lg pr-2"></i>Delete
                                                                                  </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- French resources tab content --}}
                                                <div class="tab-pane fade my-4" id="resources-fr" role="tabpanel" aria-labelledby="resources-fr-tab">
                                                    <div class="my-4 resources-content">
                                                        {{-- Message when no resources presents --}}
                                                        <div class="alert alert-info">
                                                            <p class="m-0">When no Resources : No Resources have been uploaded yet!</p>
                                                        </div>
                                                        {{-- Slide deck upload area - Embed Dropzone file upload here--}}
                                                        <form action="#" class="dropzone" id="">
                                                            <input type="file" name="file" />
                                                            <div class="my-2">
                                                                <button class="btn btn-success btn-sm">Upload</button>
                                                            </div>
                                                        </form>
                                                        {{-- Resources list --}}
                                                        <div>
                                                            <div class="table-responsive">
                                                                <table class="table table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Uploaded on</th>
                                                                            <th>Operations</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>Test.pdf</td>
                                                                            <td>2020-08-06</td>
                                                                            <td>
                                                                                <a href="#">
                                                                                    <i class="fas fa-eye fa-lg pr-2"></i>View
                                                                                  </a>
                                                                                  <span class="vr"></span>
                                                                                  <a href="#" class="red-text">
                                                                                    <i class="fas fa-trash fa-lg pr-2"></i>Delete
                                                                                  </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-qa" role="tabpanel" aria-labelledby="nav-qa-tab">
                                <div class="my-4 qa_content">
                                     {{-- Message when no question/answers presents --}}
                                     <div class="alert alert-info">
                                        <p class="m-0">When no Q/A : No questions have been asked yet!</p>
                                    </div>
                                    {{-- Questions & Answers list --}}
                                    <ul class="list-group">
                                        {{-- Dynamic : Q/A Details --}}
                                        <li class="list-group-item">
                                            {{-- Q/A Head --}}
                                            <div class="list-head d-flex flex-wrap justify-content-between align-items-center w-100">
                                                <div class="qa-info">
                                                    <p class="font-weight-bold p-0 m-0">
                                                        Some questions asked by a viewer to the speaker
                                                    </p>
                                                    {{-- Question author & Time--}}
                                                    <span class="author f-14">
                                                        <i class="fas fa-user text-muted mr-1"></i> Brian Greene
                                                    </span>
                                                    <span class="time p-2 f-14">
                                                       <i class="fas fa-clock text-muted"></i> 05:03:52 PM
                                                    </span>
                                                </div>
                                                <div class="qa-status d-flex">
                                                    {{-- View/Hide Reply --}}
                                                    <button class="btn btn-info btn-sm text-white rounded-5px" data-toggle="collapse" href="#answer-1" role="button">
                                                        <i class="fas fa-eye"></i> View Reply
                                                    </button>
                                                    {{-- Status of the question - Answer or Pending --}}
                                                    <div class="q-answered">
                                                        <i class="fas fa-clipboard-check fa-lg"></i>
                                                        <span class="f-14">Answered</span>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Q/A Collapsing Body --}}
                                            <div class="collapse w-100 answer_card" id="answer-1">
                                                <div class="border my-2 p-4">
                                                    <span class="font-weight-bold p-0 m-0 f-14">
                                                        Answer 
                                                    </span>
                                                    <span class="time p-2 f-14">
                                                        <i class="fas fa-clock text-muted"></i> 05:03:52 PM
                                                    </span>
                                                    <hr class="m-0">
                                                    <p class="pt-2 m-0 f-14">
                                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni impedit id perferendis aperiam labore dolorem vel esse sunt? Tenetur, ad?
                                                    </p>
                                                    <div class="answer_operations mt-4">
                                                        <button class="btn btn-success btn-sm"><i class="fas fa-pen"></i> <span class="pl-2">Edit</span></button>
                                                        <button class="btn btn-danger btn-sm"><i class="fas fa-trash"></i> <span class="pl-2">Delete</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        {{-- Dynamic : Q/A Details --}}
                                        <li class="list-group-item">
                                            {{-- Q/A Head --}}
                                            <div class="list-head d-flex flex-wrap justify-content-between align-items-center w-100">
                                                <div class="qa-info">
                                                    <p class="font-weight-bold p-0 m-0">
                                                        An interesting question asked by a viewer to the speaker
                                                    </p>
                                                    {{-- Question author & Time--}}
                                                    <span class="author f-14">
                                                        <i class="fas fa-user text-muted mr-1"></i> John Duddley
                                                    </span>
                                                    <span class="time p-2 f-14">
                                                       <i class="fas fa-clock text-muted"></i> 05:03:52 PM
                                                    </span>
                                                </div>
                                                <div class="qa-status d-flex">
                                                    {{-- View/Hide Reply --}}
                                                    <button class="btn btn-info btn-sm text-white rounded-5px" data-toggle="collapse" href="#answer-2" role="button">
                                                        <i class="fas fa-reply"></i> Reply
                                                    </button>
                                                    {{-- Status of the question - Answer or Pending --}}
                                                    <div class="q-pending">
                                                        <i class="fas fa-exclamation-triangle fa-lg"></i>
                                                        <span class="f-14">Pending</span>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Q/A Collapsing Body --}}
                                            <div class="collapse w-100 answer_card" id="answer-2">
                                                <div class="border my-2 p-4">
                                                    <span class="font-weight-bold p-0 m-0 f-14">
                                                        Answer 
                                                    </span>
                                                    <hr class="m-0">
                                                    <p class="pt-2 m-0 f-14">
                                                        <textarea class="form-control" cols="10" rows="5"></textarea>
                                                    </p>
                                                    <div class="answer_operations mt-4">
                                                        <button class="btn btn-success btn-sm"><i class="fas fa-pen"></i> <span class="pl-2">Submit</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )

@endsection
