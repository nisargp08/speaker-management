@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
{{-- Nisarg Edit - Please append following css to the css file used in this file - STARTS --}}
<style>
.session-summary .font-weight-600 {
	 font-weight: 600;
}
 .session-summary .f-10 {
	 font-size: 10px;
}
 .session-summary .modal-body {
	 padding: 0;
}
 .session-summary .session-header {
	 background-color: whitesmoke;
	 box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
	 padding: 2rem;
}
 .session-summary .session-header .session-title {
	 display: flex;
	 justify-content: space-between;
	 flex-wrap: wrap;
	 align-items: center;
}
 .session-summary .session-header .session-title .left-side .location {
	 font-size: 14px;
	 margin-bottom: 1rem;
	 color: #2a4365;
}
 @media (min-width: 768px) {
	 .session-summary .session-header .session-title .left-side .location {
		 margin-bottom: 0;
	}
}
 .session-summary .session-header .session-title .right-side {
	 margin-top: 20px;
	 display: flex;
	 flex-direction: column;
	 gap: 0.5rem;
}
 .session-summary .session-header .session-title .right-side .bookmark-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #2d3748;
	 color: whitesmoke;
}
 .session-summary .session-header .session-title .right-side .session-zoom {
	 margin-top: 1rem;
	 text-align: center;
}
 .session-summary .session-header .session-title .right-side .session-zoom .zoom-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #4299e1;
	 color: whitesmoke;
}
 .session-summary .session-header .session-date-time {
	 display: flex;
	 align-items: center;
	 flex-wrap: wrap;
	 font-size: 14px;
	 color: #1a202c;
}
 .session-summary .session-header .session-date-time .remind-btn {
	 background-color: #cbd5e0;
	 border-radius: 8px;
}
 .session-summary .session-header .session-speakers-wrapper .title {
	 font-weight: 600;
	 font-size: 16px;
	 color: gray;
}
 .session-summary .session-header .session-speakers {
	 display: flex;
	 flex-wrap: wrap;
}
 .session-summary .session-header .session-speakers .speaker {
	 display: flex;
	 align-items: center;
	 padding: 5px 0;
}
 .session-summary .session-header .session-speakers .speaker img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 50%;
	 width: 42px;
	 height: 42px;
}
 .session-summary .session-header .session-speakers .speaker .speaker-name {
	 margin: 0;
	 padding: 0 10px;
	 font-size: 14px;
	 color: #2c5282;
	 font-weight: 600;
}
 .session-summary .session-header .download-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 border: 1px solid #2d3748;
}
 .session-summary .session-body {
	 padding: 2rem;
}
 .session-summary .session-body h5 {
	 color: #2c5282;
}
 .session-summary .session-body ul {
	 margin-left: 2rem;
	 list-style-type: disc;
}
</style>
{{-- Nisarg Edit - Please append above css to the css file used in this file - ENDS--}}
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )

	<div class="row">
		<div class="col-sm-5 video_description">
			<p>{!! Content::get( 'virtual_portal_description' ) !!}</p>
		</div>
		
		@if ($default_video)
		<div class="col-sm-7">
			<video id='video' controls="controls" preload='none' width="100%" poster="{!! Content::get( 'virtual_portal_default_logo' ) !!}">
				<source id='mp4' src="{!! $default_video !!}" type='video/mp4' />
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
		@endif
	</div>

@endsection

@section( 'content' )
	<div id="main_body" class="container content-slide">
		<div class="row">
			@if( isset( $activities ) && !$activities->isEmpty() )
				<div class="card-deck w-100">
				@foreach( $activities as $activity )
				{{-- Nisarg Edit : Updated column spacing - From 'col-sm-4' to 'col-md-6 col-lg-4' --}}
					<div class="col-md-6 col-lg-4 small_bio">
						<div class="card ui-draggable" style="width: 100%;">
							<div class="card-img-top">

								@foreach ($activity->people as $person)
									<div class="presenter">
										{{--<img src="{{ url('images/virtual/bio-pic.png') }}" border="0" onerror="this.src='{{url( 'images/virtual/bio-pic.png' )}}'" class="presenter_img"/>--}}
										<span class="presenter_img"></span><!-- temp -->
										<div class="presenter_name">
											{{ $person->first_name }} {{ $person->last_name }}
											<a data-morphing="" data-src="#morphing-content" href="{{ route('virtual.front.bio', [$project->slug, $person->id]) }}" class="morphing-btn"><i class="fas fa-address-card"></i></a>
										</div>
									</div>
								@endforeach

							</div>
							<div class="card-body">
								<h5 class="card-title">{{ $activity->name }}</h5>
								<p class="card-text">{{ str_limit($activity->description, 255) }}</p>
								{{-- Nisarg Edit : Updating Video Button design && Adding Summary Page : STARTS--}}
								<div class="flex mt-auto">
									<div class="d-flex justify-content-center align-items-center my-2" style="gap : 1rem">
										<a data-src="{{ route( 'virtual.front.summary', [$project->slug, encrypt( $activity->id )] ) }}" title="Session summary" class="video-btn btn btn-sm loadModal"><i class="fas fa-info-circle"></i><span class="pl-2">Summary</span></a>
										<a title="Session video" href="{{ route( 'virtual.front.video_console', [$project->slug, encrypt( $activity->id )] ) }}" class="morphing-btn video-btn btn btn-sm"><i class="fas fa-video"></i><span class="pl-2">Watch Video</span></a>
									</div>
								</div>
								{{-- Nisarg Edit : Updating Video Button design && Adding Summary Page : ENDS --}}
							</div>
						</div>
					</div>
				@endforeach
				</div>
			@else
				<p class="alert alert-info">{!! Content::get( 'virtual_portal_no_sessions_message' ) !!}</p>
			@endif
		</div>
		{{-- Nisarg Edit - Session Summary Modal - STARTS --}}
		<div class="modal fade session-summary" id="sessionSummary" tabindex="-1" role="dialog" aria-labelledby="sessionSummaryLabel" aria-hidden="true"></div>
		<div class="modal fade" id="bioModal" tabindex="-1" role="dialog" aria-labelledby="bioModalLabel" aria-hidden="true"></div>
		{{-- Nisarg Edit - Session Summary Modal - ENDS --}}
	</div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('#summernote').summernote({
				tabsize: 2,
				height: 230
			});

			
			// jwplayer( "myplayer" ).setup( {
			//     "file": "",
			//     "playbackRateControls": [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2]
			// } );
		})
		.on( 'click', '.loadModal', function( event ){
			event.preventDefault();
			const url = $( this ).data( 'src' );
			$.ajax( {
				type: "GET",
				url: url,
				success: function( response )
				{
					$( '#sessionSummary' ).html( response );
					$( '#sessionSummary' ).modal( 'show' );

					$("[data-morphing]").fancyMorph({
						hash: "morphing",
						beforeLoad: function(){
							var href = $("a.morphing-btn_circle").attr('href');

							if (href) {
								$('#morphing-content').load(href, function(resp){
								});
							}
						},
						beforeClose: function(){
							$('#morphing-content').empty();
						}
					});
				}
			} );
		} )
		// toggle bookmark for user
		.on('click', '.bookmark-btn', function(e){
			e.preventDefault();

			var href = $(this).attr('href'),
				bookmarked = $(this).data('bookmarked'),
				$button = $(this);

			$.post(href, {set: (bookmarked ? 0 : 1)}, function(resp){
				$button.data('bookmarked', resp.set);

				$button.html('<i class="far fa-bookmark mr-2"></i>Bookmark Session');
				if (resp.set) {
					$button.html('<i class="fas fa-bookmark mr-2"></i>Bookmarked');
				}
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			var maxLength = 60;
			$(".show-read-more").each(function(){
				var myStr = $(this).text();
				if($.trim(myStr).length > maxLength){
					var newStr = myStr.substring(0, maxLength);
					var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
					$(this).empty().html(newStr);
					$(this).append('<br /><a href="javascript:void(0);" class="read-more">read more...</a>');
				}
			});
			$(".read-more").click(function(){
				$(this).siblings(".more-text").contents().unwrap();
				$(this).remove();
			});

			$(function () {
				$(".card").draggable({
					handle: '.card-header',
					cancel: '.card-body',
					snap: ".squaredotted",
					snapMode: "inner"
				});
				//$(".card-body").disableSelection();
			});

			$( function() {
				$( ".grid" ).sortable({
					revert: true,
					handle: '.card-header',
					distance: 10
				});
				$( ".card" ).draggable({
					handle: '.card-header',
					cancel: '.card-body',
					connectToSortable: ".grid",
					revert: "invalid"
				});
				//$(".card-body").disableSelection();
				
			} );
		});
	</script>
@endsection