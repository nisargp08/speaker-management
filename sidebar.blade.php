@php
	$menuService = new \App\Classes\MenuService;
	$virtualMenu = $project->menus()->where('type_id', config('c.TYPE_VIRTUAL'))->first();
	$menuContent = view('display.menus.show', ['menuArray' => $menuService->getMenu($virtualMenu)])->render();
@endphp

   
<div id="menu" class="panel" role="navigation" style="position: fixed; top: 0px; bottom: 0px; height: 100%; left: 0px; width: 15.625em; transition: left 300ms ease 0s;">
	<div class="menu_top">
		<span class="alliance_logo"></span> MyConference Suite
	</div>
	{!! $menuContent !!}

	<ul class="nav navbar-nav" style="margin-top:30px;">

		<li class="">

			@php
				$mUserId = session()->get('lead_user');
				$mUser = \De\Lead\LeadUser::find($mUserId);
			@endphp
            
            @if ($mUser->people)
			<a href="{{ route('virtual.front.speaker.show', [$project->slug]) }}">Speaker Dashboard</a>
			@endif
		</li>

	</ul>

</div>