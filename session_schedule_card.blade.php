@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
<style>
.session-schedule-card .session-header {
	 background-color: whitesmoke;
	 box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
	 padding: 2rem;
	 border-radius: 8px;
}
 .session-schedule-card .session-header .session-title {
	 display: flex;
	 justify-content: space-between;
	 flex-wrap: wrap;
	 align-items: center;
}
 .session-schedule-card .session-header .session-title .left-side .location {
	 font-size: 14px;
	 margin-bottom: 1rem;
	 color: #2a4365;
}
 @media (min-width: 768px) {
	 .session-schedule-card .session-header .session-title .left-side .location {
		 margin-bottom: 0;
	}
}
 .session-schedule-card .session-header .session-date-time {
	 display: flex;
	 align-items: center;
	 flex-wrap: wrap;
	 font-size: 14px;
	 color: #1a202c;
}
 .session-schedule-card .session-header .session-date-time .remind-btn {
	 background-color: #cbd5e0;
	 border-radius: 8px;
}
 .session-schedule-card .session-header .session-speakers-wrapper .title {
	 font-weight: 600;
	 font-size: 16px;
	 color: gray;
}
 .session-schedule-card .session-header .session-speakers {
	 display: flex;
	 flex-wrap: wrap;
}
 .session-schedule-card .session-header .session-speakers .speaker {
	 display: flex;
	 align-items: center;
	 padding: 5px 0;
}
 .session-schedule-card .session-header .session-speakers .speaker img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 50%;
	 width: 42px;
	 height: 42px;
}
 .session-schedule-card .session-header .session-speakers .speaker .speaker-name {
	 margin: 0;
	 padding: 0 10px;
	 font-size: 14px;
	 color: #2c5282;
	 font-weight: 600;
}
 .session-schedule-card .session-header .session-operations {
	 display: flex;
	 justify-content: space-between;
	 align-items: center;
}
 .session-schedule-card .session-header .bookmark-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #cbd5e0;
	 color: black;
}
 .session-schedule-card .session-header .summary-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #2a4365;
	 color: whitesmoke;
}
 .session-schedule-card .session-header .download-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 border: 1px solid #2d3748;
}
 .clickable-session-card {
	 color: inherit;
}
 .clickable-session-card:hover {
	 text-decoration: none;
	 color: #2c5282;
}
/* Initial Avatar css - Starts */
.initial-avatar{
    border-radius: 50%;
    width: 42px;
    height: 42px;
    background: red;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    color: white;
    font-size: 14px;
    font-weight: 600;
}
/* Initial Avatar css - Ends */

</style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )
@endsection

@section( 'content' )
<hr>
    <div class="container">
        <span class="initial-avatar">Bob Gordon</span>
        <span class="initial-avatar">David Dugas</span>
        <span class="initial-avatar">Greg Rothwell</span>
        <span class="initial-avatar">Yahya Awada</span>
        <span class="initial-avatar">Nisarg Patel</span>
    </div>
    <hr>
    <div class="container">
        <div class="d-flex flex-wrap">
            {{-- DESIGN - 1 --}}
            {{-- NOTE : You can remove the 'max-width and margin' inline styles if using in row/col or a flex container to take width according to available space --}}
            {{-- Using inline for demo purposes --}}
            <div class="session-schedule-card" style="max-width: 32rem;margin:20px 0">
                <div class="session-header">
                    {{-- Session Info --}}
                    <div class="session-title">
                        <div class="left-side">
                            <h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
                            <div class="location">	
                                <i class="fas fa-map-marker-alt"></i>
                                Plaza AB(2nd floor)
                            </div>
                            <div class="session-date-time">
                                <span class="date">
                                    <i class="fas fa-calendar-week mr-1"></i>
                                    Wednesday, 29th April
                                </span>
                                <span class="time mx-2">
                                    <i class="fas fa-clock mr-1"></i>
                                    14:15 - 17:30
                                </span>
                                <button class="btn btn-sm remind-btn">
                                    <i class="fas fa-bell"></i> 
                                    Remind Me
                                </button>
                            </div>
                        </div>
                        <button class="btn download-btn btn-sm my-2"><i class="fas fa-download mr-2"></i>Download Resources</button>
                    </div>
                    <hr>
                    {{-- Session speakers data --}}
                    <div class="session-speakers-wrapper mt-8">
                        <h5 class="title">Speakers</h5>
                        <div class="session-speakers">
                            <div class="speaker">
                                {{-- If photo present --}}
                                {{-- <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="Speaker">    --}}
                                {{-- Else default photo - initials --}}
                                <span class="initial-avatar">Anthony Gray</span>
                                {{-- Hyperlink to speakers profile --}}
                                <a href="#" target="_blank" class="speaker-name">Anthony Gray</a>
                            </div>
                            <div class="speaker">
                                {{-- If photo present --}}
                                {{-- <img src="https://randomuser.me/api/portraits/women/63.jpg" alt="Speaker">      --}}
                                {{-- Else default photo - initials --}}
                                <span class="initial-avatar">Alia Pitts</span> 
                                {{-- Hyperlink to speakers profile --}}
                                <a href="#" target="_blank" class="speaker-name">Alia Pitts</a>
                            </div>
                            <div class="speaker">
                                {{-- If photo present --}}
                                {{-- <img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="Speakers">      --}}
                                {{-- Else default photo - initials --}}
                                <span class="initial-avatar">Britney Cooper</span> 
                                {{-- Hyperlink to speakers profile --}}
                                <a href="#" target="_blank" class="speaker-name">Britney Cooper</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Session summary button --}}
                    <div class="session-operations">
                        <button class="btn summary-btn"><i class="fas fa-info-circle mr-2"></i>Summary</button>
                        <button class="btn bookmark-btn"><i class="far fa-bookmark mr-2"></i>Bookmark</button>
                    </div>
                </div>
            </div>

            <hr>
            {{-- DESIGN - 2 --}}
            {{-- NOTE : You can remove the 'max-width and margin' inline styles if using in row/col or a flex container to take width according to available space --}}
            {{-- Using inline for demo purposes --}}
            <a href="#" class="clickable-session-card">
            <div class="session-schedule-card" style="max-width: 32rem;margin:20px 0">
                <div class="session-header">
                    {{-- Session Info --}}
                    <div class="session-title">
                        <div class="left-side">
                            <h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
                            <div class="location">	
                                <i class="fas fa-map-marker-alt"></i>
                                Plaza AB(2nd floor)
                            </div>
                            <div class="session-date-time">
                                <span class="date">
                                    <i class="fas fa-calendar-week mr-1"></i>
                                    Wednesday, 29th April
                                </span>
                                <span class="time mx-2">
                                    <i class="fas fa-clock mr-1"></i>
                                    14:15 - 17:30
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Session description --}}
                    <div class="session-description">
                        {{ str_limit('Lorem ipsum dolor sit amet consectetur, adipisicing elit. Odio suscipit accusantium incidunt sed, aliquid facilis voluptas, exercitationem dolorum temporibus repudiandae tempore consequuntur libero repellat quaerat architecto vitae quam voluptatibus vel doloremque autem, totam ullam. Possimus quam laborum dicta! Adipisci excepturi temporibus sequi voluptates laboriosam amet nisi ex accusamus esse? Iure?', 255) }}
                    </div>
                </div>
            </div>
            </a>
        </div>

        <hr>
        {{-- Design - 3 - If one session per row --}}
        {{-- NOTE : Styling and markup is same as 'Design-1',only differnce there is no inline styling('max-width and margin') on the wrapper elemenet --}}
        <div class="session-schedule-card">
            <div class="session-header">
                {{-- Session Info --}}
                <div class="session-title">
                    <div class="left-side">
                        <h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
                        <div class="location">	
                            <i class="fas fa-map-marker-alt"></i>
                            Plaza AB(2nd floor)
                        </div>
                        <div class="session-date-time">
                            <span class="date">
                                <i class="fas fa-calendar-week mr-1"></i>
                                Wednesday, 29th April
                            </span>
                            <span class="time mx-2">
                                <i class="fas fa-clock mr-1"></i>
                                14:15 - 17:30
                            </span>
                            <button class="btn btn-sm remind-btn">
                                <i class="fas fa-bell"></i> 
                                Remind Me
                            </button>
                        </div>
                    </div>
                    <button class="btn download-btn btn-sm my-2"><i class="fas fa-download mr-2"></i>Download Resources</button>
                </div>
                <hr>
                {{-- Session speakers data --}}
                <div class="session-speakers-wrapper mt-8">
                    <h5 class="title">Speakers</h5>
                    <div class="session-speakers">
                        <div class="speaker">
                            <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="Speaker">   
                            {{-- Hyperlink to speakers profile --}}
                            <a href="#" target="_blank" class="speaker-name">Anthony Gray</a>
                        </div>
                        <div class="speaker">
                            <img src="https://randomuser.me/api/portraits/women/63.jpg" alt="Speaker">      
                            {{-- Hyperlink to speakers profile --}}
                            <a href="#" target="_blank" class="speaker-name">Alia Pitts</a>
                        </div>
                        <div class="speaker">
                            <img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="Speakers">     
                            {{-- Hyperlink to speakers profile --}}
                            <a href="#" target="_blank" class="speaker-name">Britney Cooper</a>
                        </div>
                    </div>
                </div>
                <hr>
                {{-- Session summary button --}}
                <div class="session-operations">
                    <button class="btn summary-btn"><i class="fas fa-info-circle mr-2"></i>Summary</button>
                    <button class="btn bookmark-btn"><i class="far fa-bookmark mr-2"></i>Bookmark</button>
                </div>
              </div>
        </div>


        <hr>
        {{-- Design - 4 - If one session per row --}}
        {{-- NOTE : Styling and markup is same as 'Design-2',only differnce there is no inline styling('max-width and margin') on the wrapper elemenet --}}
        <a href="#" class="clickable-session-card">
            <div class="session-schedule-card">
                <div class="session-header">
                    {{-- Session Info --}}
                    <div class="session-title">
                        <div class="left-side">
                            <h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
                            <div class="location">	
                                <i class="fas fa-map-marker-alt"></i>
                                Plaza AB(2nd floor)
                            </div>
                            <div class="session-date-time">
                                <span class="date">
                                    <i class="fas fa-calendar-week mr-1"></i>
                                    Wednesday, 29th April
                                </span>
                                <span class="time mx-2">
                                    <i class="fas fa-clock mr-1"></i>
                                    14:15 - 17:30
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Session description --}}
                    <div class="session-description">
                        {{ str_limit('Lorem ipsum dolor sit amet consectetur, adipisicing elit. Odio suscipit accusantium incidunt sed, aliquid facilis voluptas, exercitationem dolorum temporibus repudiandae tempore consequuntur libero repellat quaerat architecto vitae quam voluptatibus vel doloremque autem, totam ullam. Possimus quam laborum dicta! Adipisci excepturi temporibus sequi voluptates laboriosam amet nisi ex accusamus esse? Iure?', 255) }}
                    </div>
                  </div>
            </div>
        </a>
    </div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )
<script>
    //Function to get random color for user's avatar
    function getRandomColor(){
        var letters = '0123456789ABCDE'; //Dark Hex string
        var color = '#';
        // Looping to get '6' random elements from the hex string to form a hex code
        for(var i=0 ; i < 6 ; i++){
            //Getting a random number from the hex string and appending to color string
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    function getInitials(name){
        //Will replace the string which matched regex
        // Regex : \b\w - Word boundary in a word - returns first alphabets from the word
        var initials = name.replace(/[^a-zA-Z- ]/g, "").match(/\b\w/g);
        //Convert initials array into string and return
        return initials.join('');
    };

    //Fetching all elements by the class '.initial-avatar'
    let avatarList = document.querySelectorAll('.initial-avatar');
    //Looping through all of them to assign a random background color
    avatarList.forEach(function(avatar) {
        //Generate intials by calling the function
        let initials = getInitials($(avatar).text());
        //Update DOM text from full name to initials
        $(avatar).text(initials);
        //Assigning returned random color as avatar's bg
        $(avatar).css("background-color",getRandomColor());
    })

</script>
@endsection