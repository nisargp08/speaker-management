@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
	<link rel="stylesheet" href="{{ url( 'css/virtual/variables.css' ) }}">
	<link rel="stylesheet" href="{{ url( 'css/virtual/my_details.css' ) }}">
	<link rel="stylesheet" href="{{ url( 'css/virtual/main_body.css' ) }}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.1/croppie.min.css">
	<style>
		.mute {
			opacity:0.5;
		}
	</style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'content' )
	<div id="main_body" class="content-slide profile">
		<div class="container">
			@if( session()->has( 'data') )
				<div class="alert alert-{{ session('data.outcome') }}">{{ session('data.message') }}</div>
			@endif
			<div class="row">
				<div class="col-sm-3">
					<span class="registrant_name">{{ $associations['firstName']->value or '' }} {{ $associations['lastName']->value or '' }}</span>
					<div class="img-container-editable">
						@if (isset($associations['profilePicture']))
						<img border="0" class="img-thumbnail" src="{{ route('file.get', [base64_encode($associations['profilePicture']->value)]) }}" onerror="this.src='{{ url( 'images/virtual/bio-pic.png' ) }}'"/>
						<ul>
							<li>
								<label for="field[{{ $associations['profilePicture']->field_id }}]"><i class="fas fa-upload"></i></label>
								<input type="file" class="crop" accept="image/*"
										id="field[{{ $associations['profilePicture']->field_id }}]" 
										data-id="field[{{ $associations['profilePicture']->field_id }}]"
										data-crop-resizable="{{ \FD::sfv($profilePicInfo, 'cropIsResizable') }}"
										data-crop-zoomable="{{ \FD::sfv($profilePicInfo, 'cropIsZoomable') }}"
										data-crop-width="{{ \FD::sfv($profilePicInfo, 'cropViewportWidth') }}"
										data-crop-height="{{ \FD::sfv($profilePicInfo, 'cropViewportHeight') }}"
										data-crop-shape="{{ \FD::sfv($profilePicInfo, 'cropViewportShape') }}"> 
								<input type="hidden" name="field[{{ $associations['profilePicture']->field_id }}][]" class="croppedBase64" disabled="">
							</li>
						</ul>
						@else
						<img border="0" class="img-thumbnail" src="{{ url( 'images/virtual/bio-pic.png' ) }}"/>
						@endif
					</div>
					<span class="row social_connection">
						<ul>
							@if (isset($associations['twitter']) && $associations['twitter']->value)
							<li class="Twitter">
								<a target="_blank" href="{{ $associations['twitter']->value or '' }}">
									<span class="fa-stack fa-2x">
										<i class="fas fa-circle fa-stack-2x"></i>
										<i class="fas fab fa-twitter fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							@endif
							@if (isset($associations['facebook']) && $associations['facebook']->value)
							<li class="Facebook">
								<a target="_blank" href="{{ $associations['facebook']->value or '' }}">
									<span class="fa-stack fa-2x">
										<i class="fas fa-circle fa-stack-2x"></i>
										<i class="fas fab fa-facebook fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							@endif
							@if (isset($associations['linkedin']) && $associations['linkedin']->value)
							<li class="LinkedIn">
								<a target="_blank" href="{{ $associations['linkedin']->value or '' }}">
									<span class="fa-stack fa-2x">
										<i class="fas fa-circle fa-stack-2x"></i>
										<i class="fas fab fab fa-linkedin-in fa-stack-1x fa-inverse"></i>
									</span>
								</a>
							</li>
							@endif
						</ul>
					</span>
					<div class="row myDetais">
						<h3>My Details</h3>
						<div class="row">
							<div class="col name">{{ $associations['firstName']->value or '' }} {{ $associations['lastName']->value or '' }}</div>
						</div>
						<div class="row">
							<div class="col email">{{ $associations['email']->value or '' }}</div>
						</div>
						<a class="btn btn-sm btn-warning" data-morphing id="morphing" data-src="#profile" href="javascript:;"><i class="fas fa-pen-alt"></i>Update</a>
					</div>
				</div>
				<div class="col-sm-9">
					<div class=" tab_section">
						<!-- Tabbed Content -->
						<ul class="nav nav-tabs" id="profile_tabs" role="tablist">
							<li class="nav-item" role="presentation">
								<a class="nav-link" id="upcoming_sessions-tab" data-toggle="tab" href="#upcoming_sessions" role="tab"
									aria-controls="upcoming_sessions" aria-selected="false">My Sessions</a>
							</li>
							<!-- <li class="nav-item" role="presentation">
								<a class="nav-link" id="Past_Sessions-tab" data-toggle="tab" href="#Past_Sessions" role="tab"
									aria-controls="Past_Sessions" aria-selected="false">My Past Sessions</a>
							</li> -->
							<li class="nav-item" role="presentation">
								<a class="nav-link" id="My_Notes-tab" data-toggle="tab" href="#My_Notes" role="tab" aria-controls="My_Notes"
									aria-selected="false">My Notes</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" id="My_resouces-tab" data-toggle="tab" href="#My_resouces" role="tab"
									aria-controls="My_resouces" aria-selected="false">Available Resources</a>
							</li>
							<li class="nav-item" role="presentation">
								<a class="nav-link" id="My_Settings-tab" data-toggle="tab" href="#My_Settings" role="tab"
									aria-controls="My_Settings" aria-selected="false">My Settings</a>
							</li>
						</ul>

						<div class="tab-content">

							<div class="tab-pane" id="upcoming_sessions" role="tabpanel" aria-labelledby="upcoming_sessions-tab">
								<h2 class="display-4">My Sessions</h2>
								<div class="list-group">
								@if( isset( $activities ) && $activities->isNotEmpty() )
									@foreach ($activities->where('end_datetime', '>', date('Y-m-d')) as $activity)
										<a href="{{ route( 'virtual.front.video_console', [$project->slug, encrypt( $activity->id )] ) }}" class="list-group-item list-group-item-action">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">{{ $activity->name }}</h5>
											</div>
											<small>{{ \Carbon\Carbon::parse($activity->start_datetime)->diffForHumans() }}</small><br>
											{{--<small>{{ \Carbon\Carbon::parse($activity->start_datetime)->format('h:i:s A') }} - {{ \Carbon\Carbon::parse($activity->end_datetime)->format('h:i:s A') }}</small>--}}
											<hr>
											<p class="mb-1">{{ str_limit($activity->description, 255) }}</p>
										</a>
										<br>
									@endforeach

									@if ($activities->where('end_datetime', '<', date('Y-m-d'))->count() > 0)
									<h4>Past Sessions</h4>

									@foreach ($activities->where('end_datetime', '<', date('Y-m-d')) as $activity)
										<a href="{{ route( 'virtual.front.video_console', [$project->slug, encrypt( $activity->id )] ) }}" class="list-group-item list-group-item-action mute">
											<div class="d-flex w-100 justify-content-between">
												<h5 class="mb-1">{{ $activity->name }}</h5>
											</div>
											<small>{{ \Carbon\Carbon::parse($activity->start_datetime)->diffForHumans() }}</small><br>
											{{--<small>{{ \Carbon\Carbon::parse($activity->start_datetime)->format('h:i:s A') }} - {{ \Carbon\Carbon::parse($activity->end_datetime)->format('h:i:s A') }}</small>--}}
											<hr>
											<p class="mb-1">{{ str_limit($activity->description, 255) }}</p>
										</a>
										<br>
									@endforeach

									@endif
								@else
									<p class="alert alert-info">No upcoming sessions.</p>
								@endif
								</div>
							</div>

							<div class="tab-pane" id="Past_Sessions" role="tabpanel" aria-labelledby="Past_Sessions-tab">
								<h2 class="display-4">Past Sessions</h2>
								<div class="list-group">
									{{--
									<a href="#" class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">List group item heading</h5>
											<small class="badge badge-dark">3 days ago</small>
										</div>
										<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus
											varius blandit.</p>
										<small>Donec id elit non mi porta.</small>
									</a>
									<a href="#" class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">List group item heading</h5>
											<small class="badge badge-dark">3 days ago</small>
										</div>
										<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus
											varius blandit.</p>
										<small class="text-muted">Donec id elit non mi porta.</small>
									</a>
									<a href="#" class="list-group-item list-group-item-action">
										<div class="d-flex w-100 justify-content-between">
											<h5 class="mb-1">List group item heading</h5>
											<small class="badge badge-dark">3 days ago</small>
										</div>
										<p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus
											varius blandit.</p>
										<small class="text-muted">Donec id elit non mi porta.</small>
									</a>
									--}}
								</div>
							</div>

							<div class="tab-pane" id="My_Notes" role="tabpanel" aria-labelledby="My_Notes-tab">
								<h2 class="display-4">My Notes</h2>
								<ul class="list-group list-group-flush note-list">
									@if( isset( $notes ) && !$notes->isEmpty() )
										@foreach( $notes as $note )
											<li class="list-group-item">
												@if( $activities->where( 'id', $note->instance_id )->first() )
													<p>{{ $note->title }}: <strong>{{ $activities->where( 'id', $note->instance_id )->first()->name }}</strong></p>
												@else
													<p>{{ $note->title }}</p>
												@endif
												<a data-src="{{ route( 'virtual.front.notes.get', [$project->slug, $note->id] ) }}" class="btn btn-sm btn-primary btn-note loadModal">View</a>
											</li>
										@endforeach
									@else
										<p class="alert alert-info">No notes available.</p>
									@endif
								</ul>
							</div>

							<div class="tab-pane" id="My_resouces" role="tabpanel" aria-labelledby="My_resouces-tab">
								<h2 class="display-4">Available Resources</h2>
								<div class="card">
									<div class="resource_list show" id="resource_list">
										<div class="card-body">
											No resources available
											<!--
											<ul class="list-group list-group-flush">
												<li class="list-group-item"><i class="fas fa-file-pdf"></i> <span><a href="#" target="_blank">Cras justo odio Dapibus ac facilisis in Porta ac</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-alt"></i> <span><a href="#" target="_blank">Dapibus ac facilisis in</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-pdf"></i> <span><a href="#" target="_blank">Morbi leo risus</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-word"></i> <span><a href="#" target="_blank">Porta ac consectetur ac</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-powerpoint"></i> <span><a href="#" target="_blank">Vestibulum at eros</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-alt"></i> <span><a href="#" target="_blank">Dapibus ac facilisis in</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-pdf"></i> <span><a href="#" target="_blank">Morbi leo risus</a></span></li>
												<li class="list-group-item"><i class="fas fa-file-pdf"></i> <span><a href="#" target="_blank">Cras justo odio Dapibus ac facilisis in Porta ac</a></span></li>
											</ul>
											-->
										</div>
									</div>
								</div>
							</div>

							<div class="tab-pane" id="My_Settings" role="tabpanel" aria-labelledby="My_Settings-tab">
								<h2 class="display-4">My Settings</h2>
								<div class="form-check">
									No settings available
									<!--
									<ul class="tg-list">
										<li class="tg-list-item">
											<span class="toggle_label">Would you like to recieve communications on upcoming events?</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb1" type="checkbox" />
												<label class="tgl-btn" for="cb1"></label>
											</span>
										</li>
										<li class="tg-list-item">
											<span class="toggle_label">Share your social medial information?</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb2" type="checkbox" />
												<label class="tgl-btn" for="cb2"></label>
											</span>
										</li>
										<li class="tg-list-item">
											<span class="toggle_label">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb3" type="checkbox" />
												<label class="tgl-btn" for="cb3"></label>
											</span>
										</li>
										<li class="tg-list-item">
											<span class="toggle_label">Pellentesque habitant morbi tristique senectus et netus et malesuada
												fames</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb4" type="checkbox" />
												<label class="tgl-btn" for="cb4"></label>
											</span>
										</li>
										<li class="tg-list-item">
											<span class="toggle_label">NeAliquam quis lacus a arcu consectetur pretium.wsletters</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb5" type="checkbox" />
												<label class="tgl-btn" for="cb5"></label>
											</span>
										</li>
										<li class="tg-list-item">
											<span class="toggle_label">Integer nisl mi, finibus nec erat non, semper consequat
												mauris.</span>
											<span class="toggle_button">
												<input class="tgl tgl-flat" id="cb6" type="checkbox" />
												<label class="tgl-btn" for="cb6"></label>
											</span>
										</li>
									</ul>
									-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="profile" class="hidden" style="width:50%">
		<h3>Update profile Info</h3>
		<form method="POST" action="{{ route( 'virtual.front.update', [$project->slug] ) }}">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="firstName">First name</label>
				<input class="form-control" type="text" name="firstName" value="{{ $associations['firstName']->value or '' }}">
			</div>
			<div class="form-group">
				<label for="lastName">Last name</label>
				<input class="form-control" type="text" name="lastName" value="{{ $associations['lastName']->value or '' }}">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input class="form-control" type="text" name="email" value="{{ $associations['email']->value or '' }}">
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form>
	</div>

	<div class="modal fade local" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cropModalLabel">Crop your image</h5>
					<a class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</a>
				</div>
				<div class="modal-body">
					<div class="cropArea"></div>
				</div>
				<div class="modal-footer">
					<a role="button" class="btn btn-secondary" data-dismiss="modal">Close</a>
					<a id="doCrop" role="button" class="btn btn-primary">Crop</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade local" id="frontModal" tabindex="-1" role="dialog" aria-labelledby="frontModalLabel" aria-hidden="true">

@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )
	<script src="{{ config( 'c.cdn' ) }}/js/image_binder.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.1/croppie.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('#summernote').summernote({
				tabsize: 2,
				height: 230
			});

			$('#profile_tabs a:first').tab('show');

		})
		.on( 'click', '.loadModal', function( event ){
			event.preventDefault();
			const url = $( this ).data( 'src' );
			$.ajax( {
				type: "GET",
				url: url,
				success: function( response )
				{
					$( '#frontModal' ).html( response );
					$( '#frontModal' ).modal( 'show' );
					$('#summernote').summernote({
						tabsize: 2,
						height: 230
					});
				}
			} );
		} )
		.on( 'submit', '#note_form', function( e ){
			const textarea = document.getElementById( 'summernote' );
			const content = textarea.value;
			const note_id = $( textarea ).data( 'id' );

			$.ajax( {
				url: "{{ route( 'virtual.front.notes.save', [$project->slug] ) }}",
				type: 'post',
				data: { note_id: note_id, content: content }
			} ).done( function( response ){
				
			} );
		} )
		.on('change', 'input.croppedBase64', function(e){
			$('#mycs-loading-mask').show();
			$.post('{{route( "virtual.front.update_profile_picture", [$project->slug] )}}', $(this).serialize(), function(resp){
				location.reload(true);
				$('#mycs-loading-mask').hide();
			});
		});
	</script>
	<script>
		// function check_fields(){
		// 	if ( $( ".edit" ).length ) {
		// 		$('input').prop('readonly',false);
		// 		$('select').prop('disabled', false);
		// 		$('textarea').prop('readonly',false);
		// 		$('input').removeClass('edit');
		// 	}else{
		// 		$('input').prop('readonly',true);
		// 		$('select').prop('disabled', true);
		// 		$('textarea').prop('readonly',true);
		// 		$('input').addClass('edit');
		// 	}
		// }
			
		// function showPage(){
		// 	$('#loader').hide();
		// 	$('#myDiv').show();
			
		// }

		// function myFunction() {
		// 	var myVar;

		// 	myVar = setTimeout(showPage, 2000);
		// 	$('#loader').show();
			
		// 	$('.modify').hide();
		// 	//showPage();
		// }
			
		$(document).ready(function(){

			// check_fields();           

			// $(".modify").click(function() {
			// 	$(".modify").toggleClass("unlocked");
			// 	$('.modify').parent().toggleClass('edit');
			// 	check_fields();  
			// });
			
			// $('#loader').hide();

			// $('.save_details').click(function(){
			// 	$('#edit_details').css('display', 'none');

			// 	myFunction();

			// });

		
			var maxLength = 60;
			$(".show-read-more").each(function(){
				var myStr = $(this).text();
				if($.trim(myStr).length > maxLength){
					var newStr = myStr.substring(0, maxLength);
					var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
					$(this).empty().html(newStr);
					$(this).append('<br /><a href="javascript:void(0);" class="read-more">read more...</a>');
				}
			});
			$(".read-more").click(function(){
				$(this).siblings(".more-text").contents().unwrap();
				$(this).remove();
			});

			$("[data-morphing]").fancyMorph({
				hash: "morphing"
			});
			
			$(function () {
				$(".card").draggable({
					handle: '.card-header',
					cancel: '.card-body',
					snap: ".squaredotted",
					snapMode: "inner"
				});
				//$(".card-body").disableSelection();
			});

			$( function() {
				$( ".grid" ).sortable({
					revert: true,
					handle: '.card-header .move',
					distance: 10
				});
				$( ".card" ).draggable({
					handle: '.card-header .move',
					cancel: '.card-body',
					connectToSortable: ".grid",
					revert: "invalid"
				});
				//$(".card-body").disableSelection();
				
			} ); 
		});
	</script>
@endsection