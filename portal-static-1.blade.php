{{-- VIdeo button and Summary Button added with summary in modal - STATIC --}}
@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
{{-- Nisarg Edit - Please append following css to the css file used in this file - STARTS --}}
<style>
.session-summary .font-weight-600 {
	 font-weight: 600;
}
 .session-summary .f-10 {
	 font-size: 10px;
}
 .session-summary .modal-body {
	 padding: 0;
}
 .session-summary .session-header {
	 background-color: whitesmoke;
	 box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
	 padding: 2rem;
}
 .session-summary .session-header .session-title {
	 display: flex;
	 justify-content: space-between;
	 flex-wrap: wrap;
	 align-items: center;
}
 .session-summary .session-header .session-title .left-side .location {
	 font-size: 14px;
	 margin-bottom: 1rem;
	 color: #2a4365;
}
 @media (min-width: 768px) {
	 .session-summary .session-header .session-title .left-side .location {
		 margin-bottom: 0;
	}
}
 .session-summary .session-header .session-title .right-side {
	 margin-top: 20px;
	 display: flex;
	 flex-direction: column;
	 gap: 0.5rem;
}
 .session-summary .session-header .session-title .right-side .bookmark-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #2d3748;
	 color: whitesmoke;
}
 .session-summary .session-header .session-title .right-side .session-zoom {
	 margin-top: 1rem;
	 text-align: center;
}
 .session-summary .session-header .session-title .right-side .session-zoom .zoom-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #4299e1;
	 color: whitesmoke;
}
 .session-summary .session-header .session-date-time {
	 display: flex;
	 align-items: center;
	 flex-wrap: wrap;
	 font-size: 14px;
	 color: #1a202c;
}
 .session-summary .session-header .session-date-time .remind-btn {
	 background-color: #cbd5e0;
	 border-radius: 8px;
}
 .session-summary .session-header .session-speakers-wrapper .title {
	 font-weight: 600;
	 font-size: 16px;
	 color: gray;
}
 .session-summary .session-header .session-speakers {
	 display: flex;
	 flex-wrap: wrap;
}
 .session-summary .session-header .session-speakers .speaker {
	 display: flex;
	 align-items: center;
	 padding: 5px 0;
}
 .session-summary .session-header .session-speakers .speaker img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 50%;
	 width: 42px;
	 height: 42px;
}
 .session-summary .session-header .session-speakers .speaker .speaker-name {
	 margin: 0;
	 padding: 0 10px;
	 font-size: 14px;
	 color: #2c5282;
	 font-weight: 600;
}
 .session-summary .session-header .download-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 border: 1px solid #2d3748;
}
 .session-summary .session-body {
	 padding: 2rem;
}
 .session-summary .session-body h5 {
	 color: #2c5282;
}
 .session-summary .session-body ul {
	 margin-left: 2rem;
	 list-style-type: disc;
}
</style>
{{-- Nisarg Edit - Please append above css to the css file used in this file - ENDS--}}
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )

	<div class="row">
		<div class="col-sm-5 video_description">
			<p>{!! Content::get( 'virtual_portal_description' ) !!}</p>
		</div>
		
		@if ($default_video)
		<div class="col-sm-7">
			<video id='video' controls="controls" preload='none' width="100%" poster="{!! Content::get( 'virtual_portal_default_logo' ) !!}">
				<source id='mp4' src="{!! $default_video !!}" type='video/mp4' />
				<p>Your user agent does not support the HTML5 Video element.</p>
			</video>
		</div>
		@endif
	</div>

@endsection

@section( 'content' )
	<div id="main_body" class="container content-slide">
		<div class="row">
			@if( isset( $activities ) && !$activities->isEmpty() )
				<div class="card-deck">
				@foreach( $activities as $activity )
				{{-- Nisarg Edit : Updated column spacing - From 'col-sm-4' to 'col-md-6 col-lg-4' --}}
					<div class="col-md-6 col-lg-4 small_bio">
						<div class="card ui-draggable" style="width: 100%;">
							<div class="card-img-top">

								@foreach ($activity->people as $person)
									<div class="presenter">
										{{--<img src="{{ url('images/virtual/bio-pic.png') }}" border="0" onerror="this.src='{{url( 'images/virtual/bio-pic.png' )}}'" class="presenter_img"/>--}}
										<span class="presenter_img"></span><!-- temp -->
										<div class="presenter_name">
											{{ $person->first_name }} {{ $person->last_name }}
											<a data-morphing="" data-src="#morphing-content" href="{{ route('virtual.front.bio', [$project->slug, $person->id]) }}" class="morphing-btn"><i class="fas fa-address-card"></i></a>
										</div>
									</div>
								@endforeach

							</div>
							<div class="card-body">
								<h5 class="card-title">{{ $activity->name }}</h5>
								<p class="card-text">{{ str_limit($activity->description, 255) }}</p>
								{{-- Nisarg Edit : Updating Video Button design && Adding Summary Page : STARTS--}}
								<div class="flex mt-auto">
									<div class="d-flex justify-content-center align-items-center my-2" style="gap : 1rem">
										<a data-toggle="modal" href="#sessionSummary" title="Session summary" class="video-btn btn btn-sm"><i class="fas fa-info-circle"></i><span class="pl-2">Summary</span></a>
										<a title="Session video" href="{{ route( 'virtual.front.video_console', [$project->slug, encrypt( $activity->id )] ) }}" class="morphing-btn video-btn btn btn-sm"><i class="fas fa-video"></i><span class="pl-2">Watch Video</span></a>
									</div>
								</div>
								{{-- Nisarg Edit : Updating Video Button design && Adding Summary Page : ENDS --}}
							</div>
						</div>
					</div>
				@endforeach
				</div>
			@else
				<p class="alert alert-info">{!! Content::get( 'virtual_portal_no_sessions_message' ) !!}</p>
			@endif
		</div>
		{{-- Nisarg Edit - Session Summary Modal - STARTS --}}
		<div class="modal fade session-summary" id="sessionSummary" tabindex="-1" role="dialog" aria-labelledby="sessionSummaryLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
			  <div class="modal-content">
				<div class="modal-header">
				  <h5 class="modal-title" id="sessionSummaryLabel">Session Summary</h5>
				  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				  </button>
				</div>
				<div class="modal-body">
				  <div class="session-header">
					<div class="session-title">
						<div class="left-side">
							<h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
							<div class="location">	
								<i class="fas fa-map-marker-alt"></i>
								Plaza AB(2nd floor)
							</div>
							<div class="session-date-time">
								<span class="date">
									<i class="fas fa-calendar-week mr-1"></i>
									Wednesday, 29th April
								</span>
								<span class="time mx-2">
									<i class="fas fa-clock mr-1"></i>
									14:15 - 17:30
								</span>
								<button class="btn btn-sm remind-btn">
									<i class="fas fa-bell"></i> 
									Remind Me
								</button>
							</div>
						</div>
						<div class="right-side">
							<button class="btn bookmark-btn"><i class="far fa-bookmark mr-2"></i>Bookmark Session</button>
							<button class="btn download-btn"><i class="fas fa-download mr-2"></i>Download Resources</button>
							<div class="session-zoom">
								<p class="m-0 p-0 f-10">Webinar Zoom ID : 45678954</p>
								<p class="m-0 p-0 f-10">Passcode : 217502</p>
								<button class="btn zoom-btn"><i class="fas fa-video mr-2"></i>Join Zoom Webinar</button>
							</div>
						</div>
					</div>
					<hr>
					<div class="session-speakers-wrapper mt-8">
						<h5 class="title">Speakers</h5>
						<div class="session-speakers">
							<div class="speaker">
								<img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="Speaker">   
								{{-- Hyperlink to speakers profile --}}
								<a href="#" target="_blank" class="speaker-name">Anthony Gray</a>
							</div>
							<div class="speaker">
								<img src="https://randomuser.me/api/portraits/women/63.jpg" alt="Speaker">      
								{{-- Hyperlink to speakers profile --}}
								<a href="#" target="_blank" class="speaker-name">Alia Pitts</a>
							</div>
							<div class="speaker">
								<img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="Speakers">     
								{{-- Hyperlink to speakers profile --}}
								<a href="#" target="_blank" class="speaker-name">Britney Cooper</a>
							</div>
						</div>
					</div>
				  </div>
				  <div class="session-body">
						<div class="session-description">
							<h5 class="font-weight-600">Description <span class="colon">:</span></h5>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae, cumque perferendis, minus non sapiente quaerat deleniti nam aut optio error reiciendis? Totam, cum accusantium modi magni suscipit libero eaque corporis mollitia veniam error illum alias vero quis temporibus! Sunt, aspernatur aperiam quisquam, praesentium, tempore incidunt expedita earum perspiciatis quod suscipit placeat mollitia ex fuga quis. Nisi voluptatum quam maiores magnam similique impedit, ratione earum dolorum aspernatur maxime rerum consectetur adipisci, unde repudiandae est tempore, ipsa consequuntur quae amet mollitia nulla laborum! Ex, explicabo! Accusantium soluta deserunt repudiandae illo dicta impedit, ratione consectetur numquam illum quidem sapiente magnam! Molestias et fuga debitis? Eius numquam at quas saepe error blanditiis tempore eos, illum velit et, ipsa cupiditate voluptate. Fuga ipsam alias excepturi odio quae nisi dolorem dolores illum numquam obcaecati, pariatur quis nostrum ea eius deleniti beatae odit optio maiores quo blanditiis dolorum ut harum dicta! Sequi corrupti fugiat dolor explicabo culpa molestias sunt id! Perspiciatis, eos nihil. Nobis reprehenderit similique iure, quisquam dignissimos at magni repellat? Sequi doloribus molestias harum magni vero enim, expedita tenetur distinctio quae placeat iste eos! Necessitatibus, ex molestiae. Libero autem perspiciatis facilis soluta ipsam veritatis sint numquam eos. Culpa beatae unde esse blanditiis molestiae assumenda accusamus.
						</div>
					  	<hr>
						<div class="session-objectives">
							<h5 class="font-weight-600">Learning Objectives <span class="colon">:</span></h5>
							<ul>
								<li>Lorem ipsum dolor sit amet.</li>
								<li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda fugit nostrum sapiente qui! Quod quaerat voluptates dolor placeat magni officiis!</li>
								<li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, quis.</li>
							</ul>
						</div>
						<hr>
						<div class="session-evaluation">
							<h5 class="font-weight-600">Evaluation <span class="colon">:</span></h5>
							{{-- Session Evaluation Link --}}
							<p><a href="#">Click here</a> to evaluate this session</p>
						</div>
				  </div>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			  </div>
			</div>
		</div>
		{{-- Nisarg Edit - Session Summary Modal - ENDS --}}
	</div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )
	<script type="text/javascript">
		$(document).ready(function(){
			
			$('#summernote').summernote({
				tabsize: 2,
				height: 230
			});

			
			// jwplayer( "myplayer" ).setup( {
			//     "file": "",
			//     "playbackRateControls": [0.25, 0.5, 0.75, 1, 1.25, 1.5, 2]
			// } );
		});
	</script>
	<script>
		$(document).ready(function(){
			var maxLength = 60;
			$(".show-read-more").each(function(){
				var myStr = $(this).text();
				if($.trim(myStr).length > maxLength){
					var newStr = myStr.substring(0, maxLength);
					var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
					$(this).empty().html(newStr);
					$(this).append('<br /><a href="javascript:void(0);" class="read-more">read more...</a>');
				}
			});
			$(".read-more").click(function(){
				$(this).siblings(".more-text").contents().unwrap();
				$(this).remove();
			});

			$(function () {
				$(".card").draggable({
					handle: '.card-header',
					cancel: '.card-body',
					snap: ".squaredotted",
					snapMode: "inner"
				});
				//$(".card-body").disableSelection();
			});

			$( function() {
				$( ".grid" ).sortable({
					revert: true,
					handle: '.card-header',
					distance: 10
				});
				$( ".card" ).draggable({
					handle: '.card-header',
					cancel: '.card-body',
					connectToSortable: ".grid",
					revert: "invalid"
				});
				//$(".card-body").disableSelection();
				
			} );
		});
	</script>
@endsection