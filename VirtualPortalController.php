<?php

namespace De\Virtual\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Project;
use App\Role;
use App\Zoom;
use App\File;
use App\ZoomMeeting;
use App\Comments;
use App\Note;
// use App\Content;
// use App\ContentInfo;
use De\Reg\Registrant;
use De\Lead\LeadUser;
use De\Lead\Lead;
use De\Schedule\Activity;
use De\Schedule\Person;
use De\Video\Video;

use De\Virtual\VideoView;
use De\Virtual\ActivityLog;

use App\Repositories\ContentRepository;

use App\Classes\FieldService;
use App\Classes\InstanceData;
use App\Classes\FieldDisplay;
use App\Classes\BLogService;
use App\Classes\Zoom\Meeting;
use App\Classes\DataAssociations;
use De\Virtual\Service\VirtualPortalService;
use De\Schedule\Service\ActivityService;

use De\Reg\Service\RegistrantService;

use Carbon\Carbon;

use App\Facades\BLog;
use DB;
use Element;
use Content;
use Session;

class VirtualPortalController extends Controller
{

	/**
	 * @var VirtualPortalService
	 */
	protected $service;

	/**
	 * @var ActivityService
	 */
	protected $activityService;

	public function __construct(VirtualPortalService $service, ActivityService $activityService)
	{
		$this->service = $service;
		$this->activityService = $activityService;
	}

	public function home( Request $request, Project $project )
	{
		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.login', [
			'project' => $project
		] );
	}

	public function loginAs(Request $request, Project $project, $registrantId) 
	{
		
		$registrant = Registrant::find($registrantId);
		$user = $registrant->lead_user;

		if ($user) {
			$request->session()->put( 'lead_user' ,$user->id );
		}

		return redirect( route( 'virtual.front.portal', [$project->slug] ) );
	}

	public function login( Request $request, Project $project )
	{

		$this->validate( $request, [
			'email' => 'required',
			'password' => 'required',
		] );

		// use project to get correct leaduserid from the email.
		$leadUser = LeadUser::where('email', trim($request->email))->where('project_id',$project->id)->first();
		if ($leadUser) {
			$password_ok = false;
			$check = strcasecmp( $leadUser->email, $request->email );

			if($check == 0)
				$password_ok = true;

			$password_ok = Hash::check($request->password, $leadUser->password);
		
			// If the passwords match
			if ($password_ok) {
				// Set session
				$request->session()->put( 'lead_user' ,$leadUser->id );

				// Log the log in
				BLog::info("{first_name} {last_name} has logged in to the virtual portal front-end.", [
					'project_id' => $project->id,
					'type_id' => config( 'c.TYPE_VIRTUAL' ),
					'instance_id' => $leadUser->id,
					'first_name' => $leadUser->first_name,
					'last_name' => $leadUser->last_name,
				]);

				// Redirect to landing page
				return redirect( route( 'virtual.front.portal', [$project->slug] ) );
			}
		}

		return redirect( route( 'virtual.front.login', [$project->slug] ) )->with( 'data', [
			'outcome' => 'danger',
			'message' => 'Email and/or Password Incorrect.'
		] );
	}

	public function logout( Request $request, Project $project )
	{
		$request->session()->forget( 'registrant' );

		return redirect( route( 'virtual.front.login', [$project->slug] ) );
	}

	public function dashboard( Request $request, Project $project )
	{
		
		$user = $request->lead_user;

		$registrant = Registrant::find( $user->registrant_id );
		
		$fd = new FieldDisplay( Registrant::TYPE );

		$associations = $registrant->associations();
		
		$profilePicInfo = [];
		if (isset($associations['profilePicture'])) {
			$profilePicInfo = $fd->setField(\App\field::find($associations['profilePicture']->field_id));
		}

		//$videos = $this->videos( $project, $registrant );
		// if( $videos && !$videos->isEmpty() )
		// {
		// 	$notes = DB::table( 'notes' )
		// 		->where( 'project_id', $project->id )
		// 		->where( 'user_id', $user->id )
		// 		->whereIn( 'instance_id', $videos->keyBy( 'id' )->keys()->toArray() )
		// 		->where( 'active', true )
		// 		->get();
		// }

		// $activities = $user->activities()->with('translations','video_info')->get();
		$activities = $this->activityService->activitiesInLanguage($user, session('languageId') ?? $project->defaultLanguage())
								// ->where('activities.end_datetime', '>', \DB::raw('NOW()'))
								->orderBy('activities.start_datetime', 'ASC')
								->with('video_info')
								->get();

		// temp to make sure a video is linked to an activity before displaying it
		//$videos = \De\Virtual\VideoInfo::find($videos->pluck('id'));
		// dump($user->people->activities()->with('translations')->get() ?? null);
		//dump($videos);

		// dump($user->notes()->where('type_id', Activity::TYPE)->get());
		// dump($activities);
		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.dashboard', [
			'project' => $project,
			'associations' => $associations,
			'profilePicInfo' => $profilePicInfo,
			//'videos' => $videos,
			'activities' => $activities,
			'notes' => $user->notes()->where('type_id', Activity::TYPE)->get()
		] );
	}

	public function update_profile_picture(Request $request, Project $project)
	{
		$user = $request->lead_user;
		$registrant = Registrant::find( $user->registrant_id );
		
		$fs = resolve(FieldService::class);
		$fs->init(Registrant::TYPE);

		// $rs = resolve(RegistrantService::class);

		// Will always be empty for lead until we get instanceTypes working for lead
		$validation = $fs->buildValidationRulesArray($request, 'field', $project);
		// dd($request->field);
		$this->validate($request, $validation->getRules(), [], $validation->getCustomAttributes());

		$saveStatus = $fs->saveFields($request->field, $project, $registrant);

		return response()->json($saveStatus);
	}

	public function update( Request $request, Project $project )
	{
		$user = $request->lead_user;
		$registrant = Registrant::find( $user->registrant_id );

		$message['outcome'] = 'danger';
		$message['message'] = 'Info was not updated.';
		$associationsArray = [];
		foreach( $request->all() as $key => $input )
		{
			if( $key != '_token' )
			{
				$associationsArray['keys'][] = $key;
				$associationsArray['input'][$key] = $input;
			}
		}

		if( !empty( $associationsArray ) )
		{
			$regType = $registrant->instanceTypes->first();
	        $fd = new FieldDisplay( Registrant::TYPE, false, 100, collect( [$regType] ) );
	        $fields = $fd->getFields( $project, $registrant, Registrant::TYPE );

			foreach( $fields as $fieldId => $info )
			{
				if( isset( $info['var']['association']['option'] ) && in_array( $info['var']['association']['option'], $associationsArray['keys'] ) )
				{
					$selectClause = [
						['instance_id','=', $registrant->id],
						['type_id','=', Registrant::TYPE],
						['field_id', '=', $fieldId],
					];
					$updateClause = [
						'field_value' => $associationsArray['input'][$info['var']['association']['option']],
						'updated_at' => DB::raw( 'NOW()' ),
						'active' => true,
					];
					$fieldModel = DB::table( 'field_instance' )->where( $selectClause )->update( $updateClause );
				}
			}
			$message['outcome'] = 'success';
			$message['message'] = 'Info updated.';
		}
		return redirect( route( 'virtual.front.dashboard', [$project->slug] ) )->with( 'data', $message );
	}

	public function portal( Request $request, Project $project )
	{
		$user = $request->lead_user;
		$registrant = Registrant::find( $user->registrant_id );
		// $activities = $this->activities($project, $registrant);
		$activities = $this->activityService->activitiesInLanguage($project, session('languageId') ?? $project->defaultLanguage())->with('video_info','people')->get();

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.portal', [
			'project' => $project,
			'activities' => $activities,
			'default_video' => Content::get('virtual_portal_default_video')
		] );
	}

	/**
	 * Using the project and registrant, return the activities that the given
	 * registrant (determined through the leaduser) should have access to
	 * as an attendee.
	 * @param  Project $project
	 * @param  Registrant $registrant
	 * @return Collection
	 */
	// private function activities($project, $registrant)
	// {
	// 	return $this->activityService->activitiesInLanguage($project, session('languageId') ?? $project->defaultLanguage())->with('video_info','people')->whereIn('video_info_id', $this->videos($project, $registrant)->pluck('id'))->get();
	// }

	// private function videos( $project, $registrant )
	// {
 //        // get videos
 //        $activities = $project->activities()->where('active', true)->with('video_info')->get();
	// 	$videos = DB::table( 'videos' )->where( 'project_id', $project->id )->where( 'active', true )->get();

	// 	// filter videos if linked to registration
	// 	// only get videos available for registrant based on fields selection
 //        $fd = new FieldDisplay( Registrant::TYPE );
 //        $fd->setFilterList( ['video_field' => true] );
 //        $fields = $fd->getFields( $project, $registrant, Registrant::TYPE );
 //        if( !$fields->isEmpty() )
 //        {
	// 	    $ids = [];
	//         if( !$videos->isEmpty() )
	//         {
	//         	foreach( $videos as $link )
	//         	{
	//         		if( $link->field_value_id && $link->field_value_id != '' )
	//         		{
	//         			if( isset( $fields[$link->field_id]['saved'] ) )
	//         			{
	//         				if( is_array( $fields[$link->field_id]['saved'] ) && in_array( $link->field_value_id, $fields[$link->field_id]['saved'] ) )
	//         					$ids[] = $link->id;
	//         				elseif( $link->field_value_id == $fields[$link->field_id]['saved'] )
	//         					$ids[] = $link->id;
	//         			}		
	//         		}
	//         		else
	//         			$ids[] = $link->id;
	//         	}
	//         }
	//         $videos = $videos->whereIn( 'id', $ids );
 //        }

 //        // If dates (from/till) are set, get videos within the date range
 //        foreach( $videos as $key => $video )
 //        {
 //        	if($video->available_from)
 //        	{
 //        		if( strtotime( date( "Y-m-d H:i" ) ) <= strtotime( $video->available_from ) )
 //        			$videos->forget( $key );
 //        	}


 //        	if( $video->available_till )
 //        	{
 //        		if( strtotime( date( "Y-m-d H:i" ) ) >= strtotime( $video->available_till ) )
 //        			$videos->forget( $key );
 //        	} 
 //        }

 //        return $videos;
	// }

	public function video_console( Request $request, Project $project, $activity_id )
	{

		$activity = Activity::where('id', decrypt($activity_id))->first();

		$user = $request->lead_user;
		$registrant = Registrant::find( $user->registrant_id );

        $fd = new FieldDisplay( Registrant::TYPE );
		$associations['email'] = $fd->getValueByName( $registrant, ['association' => 'email'] );
		$associations['firstName'] = $fd->getValueByName( $registrant, ['association' => 'firstName'] );
		$associations['lastName'] = $fd->getValueByName( $registrant, ['association' => 'lastName'] );

		$video_url = '';
		$note = null;
		$video = null;
		$comments = null;
		$allow_likes = false;
		$allow_comments = false;
		$english_video_url = null;
		$french_video_url = null;
		$allow_likes = null;
        $allow_comments = null;
        $allow_skipping = null;
        $video_data = null;
        $video_is_available = true;
		if( $activity->video_info )
		{
			$video = $activity->video_info;

			$video_data = $this->log_video( $project, $user, $video );

			$allow_likes = $video->allow_likes;
            $allow_comments = $video->allow_comments;
            $allow_skipping = $video->allow_skipping;
            $english_video_url = '';
            $french_video_url = '';

			if( $video->video_id )
			{
				$file = File::find( $video->video_id );
		    	$path = str_replace( 'html/asset/video/', '', $file->path );
		    	$english_video_url = config( 'c.cdn' ).'/'.$path;

		    	if( $video->french_video_id != '' )
		    	{
		    		$file = File::find( $video->french_video_id );
		    		$path = str_replace( 'html/asset/video/', '', $file->path );
		    		$french_video_url = config( 'c.cdn' ).'/'.$path;
		    	}
		    

			}
			else
			{
				$english_video_url = $video->url;
				$french_video_url = $video->french_url;
			}

			$comments = Comments::where( 'type_id', config( 'c.TYPE_VIRTUAL' ) )
		    	->where( 'project_id', $project->id )
		    	->where( 'instance_id', $video->id )
		    	->where( 'reply', false )
		    	->where( 'active', true )
		    	->orderBy( 'created_at', 'desc' )
		    	->get();

	    	if($video->available_from) {
	    		$video_is_available = strtotime(date("Y-m-d H:i")) >= strtotime($video->available_from);
        	}

        	if( $video->available_till ){
	    		$video_is_available = strtotime(date("Y-m-d H:i")) <= strtotime($video->available_till);
        	} 

		}
		// dd($activity);
		if ($activity->notes()->count() > 0) {
			$note = $activity->notes()->where('user_id', $user->id)->first();
		}

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.video_console', [
			'project' => $project,
			'user' => $user,
			'associations' => $associations,
			'video' => $video,
			'activity_roles' => $project->activity_roles()->where('active', true)->get()->keyBy('id'),
			'activity' => $activity,
			'english_video_url' => $english_video_url,
			'french_video_url' => $french_video_url,
			'comments' => $comments,
			'note' => $note,
			'allow_comments' => $allow_comments,
			'allow_likes' => $allow_likes,
			'allow_skipping' => $allow_skipping,
            'video_data' => $video_data,
            'video_is_available' => $video_is_available
		] );
	}

	public function agenda( Request $request, Project $project )
	{
		$user = $request->lead_user;
		$activities = $this->activityService->activitiesInLanguage($user, session('languageId') ?? $project->defaultLanguage())->get();
		$calendar = $this->activityService->makeCalendar($project, $activities);

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.agenda', [
			'project' => $project,
			'agenda' => $agenda ?? null,
			'calendar' => $calendar
		] );
	}

	public function schedule( Request $request, Project $project )
	{
		$user = $request->lead_user;

		$calendar = $this->activityService->getFullCalendar($project, session('languageId') ?? $project->defaultLanguage());

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.schedule', [
			'project' => $project,
			'calendar' => $calendar
		] );
	}

	public function eposter ( Request $request, Project $project )
	{
		$abs = $project->remoteApi('abstract');
		
		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.eposter', [
			'project' => $project,
			'abs' => $abs
		] );
	}

	public function evaluation ( Request $request, Project $project )
	{
		$abs = $project->remoteApi('abstract');

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.evaluation', [
			'project' => $project,
			'abs' => $abs,
			'registrant' => $request->registrant
		] );
	}

	public function schedule_frame ( Request $request, Project $project )
	{
		$abs = $project->remoteApi('abstract');

		Element::prepareAll($project, config('c.TYPE_VIRTUAL'));
		return view( 'virtual::public.schedule_frame', [
			'project' => $project,
			'abs' => $abs,
			'registrant' => $request->registrant
		] );
	}

	/**
	 * Get a single speaker bio from the abstract system.
	 * @return View
	 */
	public function bio ( Request $request, Project $project, Person $person )
	{
		$lang = session('languageId') ?? $project->defaultLanguage();
		$translation = $person->translations->where('language_id', $lang)->first();

		return view('virtual::public.bio', [
			'person' => $person,
			'translation' => $translation,
			'activities' => $this->activityService->activitiesInLanguage($person, $lang)->get(),
			'file' =>  '',
			'fileUrl' =>  ''
		]);

	}

	public function speaker( Request $request, Project $project )
	{
		$user = $request->registrant;
		$video_id = [];
		$speaker = DB::table( 'users_speakers' )->where( 'user_id', $user->id )->get()->first();
		if( $speaker && $speaker->video_id )
			$video_id = json_decode( $speaker->video_id );

		$videos = DB::table( 'videos' )->where( 'project_id', $project->id )->where( 'active', true )->get();

		return view( 'virtual::public.users.speaker', [
			'client' => $project->client,
			'project' => $project,
			'user' => $user,
			'speaker' => $speaker,
			'videos' => $videos,
			'video_id' => $video_id
		] );
	}

	private function log_video( $project, $user, $video )
	{
		$data = VideoView::where( [
					['project_id', $project->id],
					['video_id', $video->id],
					['leaduser_id', $user->id],
					['active', true]] )
				->first();


		if( !$data )
		{
			$data = new VideoView();
			$data->project_id = $project->id;
			$data->video_id = $video->id;
			$data->leaduser_id = $user->id;
			$data->save();
		}

        if( session::has( "videokey" ) )
        	session::forget( "videokey" );

        Session::put( "videokey", strtotime( date( "Y-m-d H:i" ) ) );

        //prepare log
        $this->createLog( $video->id, $user->id, 'accessed' );

        $meta = [session::get( 'videokey' ) => [
        	'duration' => 0.0 , 
        	'started_at' => $data->play_duration, 
        	'pause_count' => 0, 
        	'buffered_till' => 0.0, 
        	'seek_count' => 0, 
        	'stopped_at'=> 0.0, 
        	'watch_percentage'=> 0
        ]];

        $db_meta = json_decode( $data->meta, true );

        if( !is_null( $db_meta ) && is_array( $db_meta ) )
        {
            if( count( $db_meta ) > 15 )
            	array_splice( $db_meta, 0, count( $db_meta ) - 3 );

            $db_meta[session::get( 'videokey' )] = [
            	'duration' 			=> 0.0 , 
            	'started_at' 		=> $data->play_duration, 
            	'pause_count' 		=> 0, 
            	'buffered_till' 	=> 0.0, 
            	'seek_count' 		=> 0, 
            	'stopped_at'		=> 0.0, 
            	'watch_percentage' 	=> 0
            ];
        }
        else
        {
            $db_meta = [session::get( 'videokey' ) => [
            	'duration' 			=> 0.0 , 
            	'started_at' 		=> $data->play_duration, 
            	'pause_count' 		=> 0,
            	'buffered_till' 	=> 0.0, 
            	'seek_count' 		=> 0, 
            	'stopped_at'		=> 0.0, 
            	'watch_percentage'	=> 0
            ]];
        }

        $data->meta = json_encode( $db_meta );
        $data->save();

        return $data;
	}

	private function createLog( $video, $user, $action )
    {
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $ip = $this->getUserIpAddr();

        $log = new ActivityLog();
        $log->activity_id = $video;
        $log->leaduser_id = $user;
        $log->action = $action;
        $log->ip = $ip;
        $log->user_agent = $browser;
        $log->created_at = Carbon::now();
        $log->save();
    }

    private function getUserIpAddr()
    {
    	//ip from share internet
	    if( !empty( $_SERVER['HTTP_CLIENT_IP'] ) )
	    	$ip = $_SERVER['HTTP_CLIENT_IP'];

	    //ip pass from proxy
	    elseif( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
	    	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else
	    	$ip = $_SERVER['REMOTE_ADDR'];

	    return $ip;
	}
}
