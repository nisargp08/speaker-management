@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
<style>
.attendee_list .f-14 {
	 font-size: 14px;
}
 .attendee_list .font-weight-600 {
	 font-weight: 600;
}
 .attendee_list .text-warning {
	 color: #ffc107;
}
 .attendee_list .title {
	 font-weight: 600;
	 padding: 1.5rem 0 0.5rem 0;
}
 .attendee_list .letters-container {
	 display: flex;
	 flex-wrap: wrap;
	 align-items: center;
	 justify-content: center;
}
 .attendee_list .letters-container a {
	 background-color: #a0aec0;
	 color: white;
	 padding: 5px 10px;
	 font-weight: 600;
	 border-radius: 5px;
	 margin: 2px;
}
 .attendee_list .letters-container a:hover {
	 background-color: #718096;
	 text-decoration: none;
}
 .attendee_list .letters-container a.active {
	 background-color: #2d3748;
}
 .attendee_list .alphabet-container {
	 margin: 1rem 0;
}
 .attendee_list .alphabet-container .alphabet-title {
	 display: inline-block;
	 background: #2d3748;
	 color: white;
	 font-size: 3rem;
	 font-weight: 700;
	 padding: 0 1rem;
	 border-radius: 0.5rem;
}
 .attendee_list .alphabet-container .attendee-list {
	 padding: 1rem;
	 background-color: whitesmoke;
	 border-radius: 8px;
}
 @media (min-width: 1024px) {
	 .attendee_list .alphabet-container .attendee-list {
		 padding: 2rem;
	}
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item {
	 display: flex;
	 flex-wrap: wrap;
	 align-items: center;
	 justify-content: space-between;
	 margin: 0.5rem 0;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .left-side {
	 display: flex;
	 align-items: center;
	 justify-content: center;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .left-side .avatar {
	 width: 64px;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .left-side .avatar img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 9999px;
	 object-fit: contain;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .left-side .info {
	 padding-left: 1rem;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side {
	 margin: 20px 0;
}
 @media (min-width: 1024px) {
	 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side {
		 margin: 0px;
	}
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .message, .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .contact {
	 background-color: #3182ce;
	 padding: 10px 20px;
	 border-radius: 8px;
	 color: white;
	 font-size: 14px;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .message:hover, .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .contact:hover {
	 text-decoration: none;
	 background-color: #2768a5;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .contact {
	 background-color: #343a40;
}
 .attendee_list .alphabet-container .attendee-list .attendee-list-item .right-side .contact:hover {
	 text-decoration: none;
	 background-color: #1d2124;
}
 .attendee_list .alert-title {
	 padding: 13px 16px 0px 13px;
	 color: rgba(0, 0, 0, .65);
	 font-weight: 600;
	 font-size: 27px;
	 text-align: center;
}
 .attendee_list .alert-text {
	 font-size: 16px;
	 padding: 0 10px;
	 color: rgba(0, 0, 0, .64);
	 max-width: calc(100% - 20px);
	 overflow-wrap: break-word;
}
</style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )
	<h2>Event Title</h2>
@endsection

@section( 'content' )
    <div class="container attendee_list">
        <h2 class="title">Attendee List</h2>
        <hr>
        <div class="letters-container">
            {{-- '#' shows the 'active' state.You can apply the class 'active' to active alphabet to change its stlying color --}}
            <a href="#" title="All" class="active">#</a>
            @foreach (range('a','z') as $letter)
                <a class="alpha-{{$letter}}" href="#">{{ ucfirst($letter) }}</a>
            @endforeach
        </div>
        <hr>
        {{-- Filter dropdown - Can filter by first name,last name and company --}}
        <div class="list-filter text-right">
            <div class="dropdown">
                <button class="btn btn-dark btn-sm dropdown-toggle" type="button" id="filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-filter"></i><span class="pl-2">Filter</span>
                </button>
                <div class="dropdown-menu dropdown-menu-right f-14" aria-labelledby="filter">
                    <a href="#" class="dropdown-item" title="Sort by first name"><i class="fas fa-user-alt"></i><span class="pl-2">First Name</span></a>
                    <a href="#" class="dropdown-item" title="Sort by last name"><i class="fas fa-user-alt"></i><span class="pl-2">Last Name</span></a>
                    <a href="#" class="dropdown-item" title="Sort by company name"><i class="fas fa-building"></i><span class="pl-2">Company Name</span></a>
                </div>
            </div>
        </div>
        @foreach (range('a','z') as $letter)
        <div class="alphabet-container">
            <h2 class="alphabet-title">{{ ucfirst($letter) }}</h2>
            <div class="attendee-list">
                <div class="attendee-list-item">
                    <div class="left-side">
                        <div class="avatar">
                            {{-- Avatar --}}
                            <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">   
                        </div>
                        <div class="info">
                            {{-- Hyper link to users profile --}}
                            <a href="#">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Anthony</span>
                                <span class="lname f-14 font-weight-600">Gray</span>
                           </a>
                           {{-- Company Name --}}
                            <p class="f-14 m-0">ABC Company</p>
                        </div>
                    </div>
                    <div class="right-side">
                        {{-- Share contact option --}}
                        <a class="contact" data-toggle="modal" href="#shareContact" title="Share my contact information with the user"><i class="fas fa-id-card-alt"></i> Share my contact</a>
                        {{-- Chat Option --}}
                        <a class="message" href="#" title="Send a private message to the user"><i class="fas fa-comments"></i> Message</a>
                    </div>
                </div>
                <div class="attendee-list-item">
                    <div class="left-side">
                        <div class="avatar">
                            {{-- Avatar --}}
                            <img src="https://randomuser.me/api/portraits/women/63.jpg" alt="User">   
                        </div>
                        <div class="info">
                           {{-- Hyper link to users profile --}}
                            <a href="#">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Alia</span>
                                <span class="lname f-14 font-weight-600">Pitts</span>
                            </a>
                            {{-- Company Name --}}
                            <p class="f-14 m-0">ABC Company</p>
                        </div>
                    </div>
                    <div class="right-side">
                        {{-- Share contact option --}}
                        <a class="contact" data-toggle="modal" href="#shareContact" title="Share my contact information with the user"><i class="fas fa-id-card-alt"></i> Share my contact</a>
                        {{-- Chat Option --}}
                        <a class="message" href="#"><i class="fas fa-comments"></i> Message</a>
                    </div>
                </div>
                <div class="attendee-list-item">
                    <div class="left-side">
                        <div class="avatar">
                            {{-- Avatar --}}
                            <img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="User">   
                        </div>
                        <div class="info">
                            {{-- Hyper link to users profile --}}
                            <a href="#">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Britney</span>
                                <span class="lname f-14 font-weight-600">Cooper</span>
                            </a>
                            {{-- Company Name --}}
                            <p class="f-14 m-0">ABC Company</p>
                        </div>
                    </div>
                    <div class="right-side">
                        {{-- Share contact option --}}
                        <a class="contact" data-toggle="modal" href="#shareContact" title="Share my contact information with the user"><i class="fas fa-id-card-alt"></i> Share my contact</a>
                        {{-- Chat Option --}}
                        <a class="message" href="#"><i class="fas fa-comments"></i> Message</a>
                    </div>
                </div>
                <div class="attendee-list-item">
                    <div class="left-side">
                        <div class="avatar">
                            {{-- Avatar --}}
                            <img src="https://randomuser.me/api/portraits/men/86.jpg" alt="User">   
                        </div>
                        <div class="info">
                            {{-- Hyper link to users profile --}}
                            <a href="#">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Brad</span>
                                <span class="lname f-14 font-weight-600">Newman</span>
                            </a>
                            {{-- Company Name --}}
                            <p class="f-14 m-0">ABC Company</p>
                        </div>
                    </div>
                    <div class="right-side">
                        {{-- Share contact option --}}
                        <a class="contact" data-toggle="modal" href="#shareContact" title="Share my contact information with the user"><i class="fas fa-id-card-alt"></i> Share my contact</a>
                        {{-- Chat Option --}}
                        <a class="message" href="#"><i class="fas fa-comments"></i> Message</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        {{-- Share my contact information - Modal --}}
        <div class="modal fade" id="shareContact" tabindex="-1" role="dialog" aria-labelledby="shareContactLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="shareContactLabel">Share Contact Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fas fa-exclamation-triangle fa-3x text-warning"></i>
                        <h3 class="alert-title mt-2">Are you sure ?</h3>
                        <p class="alert-text">Would you like to share your contact details with the user ?</p>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {{-- Can show success alert on confirm --}}
                <button type="button" class="btn btn-success">Confirm</button>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )

@endsection
