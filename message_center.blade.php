@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
<style>
.message_center .f-10 {
	 font-size: 10px;
}
 .message_center .f-14 {
	 font-size: 14px;
}
 .message_center .font-weight-600 {
	 font-weight: 600;
}
 .message_center .title {
	 font-weight: 600;
	 padding: 1.5rem 0 0.5rem 0;
}
 .message_center .message_card {
	 border-radius: 8px;
	 background-color: white;
	 box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
}
 .message_center .message_card .left-side, .message_center .message_card .right-side {
	 display: flex;
	 flex-direction: column;
	 height: 75vh;
	 overflow-y: hidden;
}
 .message_center .message_card .left-side .search-box, .message_center .message_card .right-side .search-box, .message_center .message_card .left-side .send-box, .message_center .message_card .right-side .send-box {
	 background-color: #fafafa;
	 padding: 10px 13px;
}
 .message_center .message_card .left-side .search-box .search-wrapper, .message_center .message_card .right-side .search-box .search-wrapper, .message_center .message_card .left-side .send-box .search-wrapper, .message_center .message_card .right-side .send-box .search-wrapper, .message_center .message_card .left-side .search-box .send-wrapper, .message_center .message_card .right-side .search-box .send-wrapper, .message_center .message_card .left-side .send-box .send-wrapper, .message_center .message_card .right-side .send-box .send-wrapper {
	 background-color: white;
	 border-radius: 20px;
	 display: flex;
	 align-items: center;
	 justify-content: space-between;
	 padding: 10px;
}
 .message_center .message_card .left-side .search-box .search-wrapper input, .message_center .message_card .right-side .search-box .search-wrapper input, .message_center .message_card .left-side .send-box .search-wrapper input, .message_center .message_card .right-side .send-box .search-wrapper input, .message_center .message_card .left-side .search-box .send-wrapper input, .message_center .message_card .right-side .search-box .send-wrapper input, .message_center .message_card .left-side .send-box .send-wrapper input, .message_center .message_card .right-side .send-box .send-wrapper input {
	 background-color: transparent;
	 border: none;
	 flex: 1;
}
 .message_center .message_card .left-side .search-box .search-wrapper input:focus, .message_center .message_card .right-side .search-box .search-wrapper input:focus, .message_center .message_card .left-side .send-box .search-wrapper input:focus, .message_center .message_card .right-side .send-box .search-wrapper input:focus, .message_center .message_card .left-side .search-box .send-wrapper input:focus, .message_center .message_card .right-side .search-box .send-wrapper input:focus, .message_center .message_card .left-side .send-box .send-wrapper input:focus, .message_center .message_card .right-side .send-box .send-wrapper input:focus {
	 outline: none;
}
 .message_center .message_card .left-side .search-box .search-wrapper input::placeholder, .message_center .message_card .right-side .search-box .search-wrapper input::placeholder, .message_center .message_card .left-side .send-box .search-wrapper input::placeholder, .message_center .message_card .right-side .send-box .search-wrapper input::placeholder, .message_center .message_card .left-side .search-box .send-wrapper input::placeholder, .message_center .message_card .right-side .search-box .send-wrapper input::placeholder, .message_center .message_card .left-side .send-box .send-wrapper input::placeholder, .message_center .message_card .right-side .send-box .send-wrapper input::placeholder {
	 color: #e3e3e3;
	 font-weight: 300;
	 margin-left: 20px;
	 font-size: 14px;
}
 .message_center .message_card .left-side .auth-user, .message_center .message_card .right-side .auth-user, .message_center .message_card .left-side .user-list, .message_center .message_card .right-side .user-list, .message_center .message_card .left-side .active-chat-user, .message_center .message_card .right-side .active-chat-user {
	 display: flex;
	 align-items: center;
	 background-color: #eee;
	 padding: 10px 15px;
	 border-top-left-radius: 8px;
}
 .message_center .message_card .left-side .auth-user .avatar, .message_center .message_card .right-side .auth-user .avatar, .message_center .message_card .left-side .user-list .avatar, .message_center .message_card .right-side .user-list .avatar, .message_center .message_card .left-side .active-chat-user .avatar, .message_center .message_card .right-side .active-chat-user .avatar {
	 width: 64px;
}
 .message_center .message_card .left-side .auth-user .avatar img, .message_center .message_card .right-side .auth-user .avatar img, .message_center .message_card .left-side .user-list .avatar img, .message_center .message_card .right-side .user-list .avatar img, .message_center .message_card .left-side .active-chat-user .avatar img, .message_center .message_card .right-side .active-chat-user .avatar img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 9999px;
}
 .message_center .message_card .left-side .auth-user .info, .message_center .message_card .right-side .auth-user .info, .message_center .message_card .left-side .user-list .info, .message_center .message_card .right-side .user-list .info, .message_center .message_card .left-side .active-chat-user .info, .message_center .message_card .right-side .active-chat-user .info {
	 padding-left: 1rem;
	 flex: 1;
}
 .message_center .message_card .left-side .users-list {
	 overflow-y: auto;
}
 .message_center .message_card .left-side .user-list {
	 background-color: white;
	 border-radius: 0;
	 border-bottom: 1px solid #eee;
}
 .message_center .message_card .left-side .user-list .unread .count {
	 background-color: #00af9c;
	 font-size: 12px;
	 color: white;
	 padding: 5px 10px;
	 border-radius: 50%;
	 font-weight: 600;
}
 .message_center .message_card .right-side .active-chat-user {
	 border-top-left-radius: 0;
	 border-top-right-radius: 8px;
}
 .message_center .message_card .right-side .send-box {
	 background-color: #eee;
}
 .message_center .message_card .right-side .active-message-window {
	 display: flex;
	 flex-direction: column;
	 align-items: flex-start;
	 flex: 1;
	 overflow-y: auto;
	 background-image: url("https://events.myconferencesuite.com/files/download/file/eyJpdiI6Ik9cL3ZwR3BVVkxYdjl5b2w3VEJRYTRRPT0iLCJ2YWx1ZSI6IkxTekpGYWZGaHF1Y3pSbTR3MnRpbmc9PSIsIm1hYyI6IjQ4ZjAyZWJjMTAwOTViOTNkMjk3ZDIwZTg5ODVjMWRlNjI2ZmNiNThlM2FjNzQ5MTlkMzczNTQ3NjdhMzY5N2YifQ==?action=preview");
}
 .message_center .message_card .right-side .active-message-window .speech-bubble-left, .message_center .message_card .right-side .active-message-window .speech-bubble-right {
	 position: relative;
	 align-self: flex-start;
	 background: #eee;
	 font-size: 14px;
	 border-radius: 0.4em;
	 margin: 10px 20px;
	 padding: 5px 15px;
	 max-width: 75%;
}
 .message_center .message_card .right-side .active-message-window .speech-bubble-left:after {
	 content: "";
	 position: absolute;
	 left: 0;
	 top: 50%;
	 width: 0;
	 height: 0;
	 border: 12px solid transparent;
	 border-right-color: #eee;
	 border-left: 0;
	 border-bottom: 0;
	 margin-top: -2px;
	 margin-left: -7px;
}
 .message_center .message_card .right-side .active-message-window .speech-bubble-right {
	 align-self: flex-end;
}
 .message_center .message_card .right-side .active-message-window .speech-bubble-right:after {
	 content: "";
	 position: absolute;
	 right: 0;
	 top: 50%;
	 width: 0;
	 height: 0;
	 border: 12px solid transparent;
	 border-left-color: #eee;
	 border-right: 0;
	 border-bottom: 0;
	 margin-top: -2px;
	 margin-right: -7px;
}
 .message_center .message_card .right-side .active-message-window .chat-date-wrapper {
	 width: 100%;
	 font-size: 14px;
	 text-align: center;
}
 .message_center .message_card .right-side .active-message-window .chat-date-wrapper .date {
	 background: #e2e8f0;
	 border-radius: 0.4em;
	 padding: 5px 10px;
	 margin: 5px 0px;
	 display: inline-block;
}
 .message_center .message_card .right-side .default-message {
	 display: flex;
	 flex: 1;
	 align-items: center;
	 justify-content: center;
}
 .message_center .message_card .right-side .default-message h3 {
	 font-weight: 300;
	 font-size: 2rem;
}
</style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )
	<h2>Event Title</h2>
@endsection

@section( 'content' )
    <div class="container message_center">
        <h2 class="title">Message Center</h2>
        <hr>
        <div class="row no-gutters message_card">
            {{-- User list --}}
            <div class="col-md-4 left-side border-right">
                {{-- Authenticated user info --}}
               <div class="auth-user">
                    <div class="avatar">
                        {{-- Avatar --}}
                        <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">   
                    </div>
                    <div class="info">
                        {{-- First,last name --}}
                        <span class="fname f-14 font-weight-600">Anthony</span>
                        <span class="lname f-14 font-weight-600">Gray</span>
                        {{-- Company Name --}}
                        <p class="f-14 m-0">ABC Company</p>
                    </div>
               </div>
               {{-- Search bar : To search users - Remove if not needed --}}
               <div class="search-box">
                   <div class="search-wrapper">
                       <i class="fas fa-search mx-2"></i>
                       <input type="text" placeholder="Search users">
                   </div>
               </div>
                {{-- Other users list --}}
                <div class="users-list">
                    {{-- Looping for demo purposes --}}
                    @for ($i = 0; $i < 5; $i++)
                        <div class="user-list">
                            <div class="avatar">
                                {{-- Avatar --}}
                                <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">   
                            </div>
                            <div class="info">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Anthony</span>
                                <span class="lname f-14 font-weight-600">Gray</span>
                                {{-- Company Name --}}
                                <p class="f-14 m-0">ABC Company</p>
                            </div>
                        </div>
                        <div class="user-list">
                            <div class="avatar">
                                {{-- Avatar --}}
                                <img src="https://randomuser.me/api/portraits/women/63.jpg" alt="User">      
                            </div>
                            <div class="info">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Alia</span>
                                <span class="lname f-14 font-weight-600">Pitts</span>
                                {{-- Company Name --}}
                                <p class="f-14 m-0">ABC Company</p>
                            </div>
                            <div class="unread">
                                <span class="count">{{$i + 1}}</span>
                            </div>
                        </div> 
                        <div class="user-list">
                            <div class="avatar">
                                {{-- Avatar --}}
                                <img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="User">         
                            </div>
                            <div class="info">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Britney</span>
                                <span class="lname f-14 font-weight-600">Cooper</span>
                                {{-- Company Name --}}
                                <p class="f-14 m-0">ABC Company</p>
                            </div>
                        </div> 
                        <div class="user-list">
                            <div class="avatar">
                                {{-- Avatar --}}
                                <img src="https://randomuser.me/api/portraits/men/86.jpg" alt="User">        
                            </div>
                            <div class="info">
                                {{-- First,last name --}}
                                <span class="fname f-14 font-weight-600">Brad</span>
                                <span class="lname f-14 font-weight-600">Newman</span>
                                {{-- Company Name --}}
                                <p class="f-14 m-0">ABC Company</p>
                            </div>
                        </div>  
                    @endfor
                </div>
            </div>
            <div class="col-md-8 right-side">
                {{-- Replace the condition and display it when a user is selected else display defualt message --}}
                @if (true)                    
                <div class="active-chat-user">
                    <div class="avatar">
                        {{-- Avatar --}}
                        <img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">   
                    </div>
                    <div class="info">
                        {{-- First,last name --}}
                        {{-- Hyperlink to users profile --}}
                        <a href="#" target="_blank">
                            <span class="fname f-14 font-weight-600">Anthony</span>
                            <span class="lname f-14 font-weight-600">Gray</span>
                        </a>
                        {{-- Company Name --}}
                        <p class="f-14 m-0">ABC Company</p>
                    </div>
                </div>
                <div class="active-message-window">
                    <div class="chat-date-wrapper">
                        <span class="date">2020-11-02</span>
                    </div>
                    <div class="speech-bubble-left">
                        <p class="m-0">Hi! 😃</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-right">
                        <p class="m-0">Hey,What's up !</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-left">
                        <p class="m-0">Lorem ipsum dolor sit amet consectetur, adipisicing elit. In, quo?</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="chat-date-wrapper">
                        <span class="date">2020-11-03</span>
                    </div>
                    <div class="speech-bubble-right">
                        <p class="m-0">Lorem ipsum dolor sit amet.</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-left">
                        <p class="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias quidem eum autem cupiditate odio voluptatem laudantium pariatur aperiam, tempora eveniet nam corrupti aliquid vero doloribus.</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-right">
                        <p class="m-0">Lorem ipsum dolor sit amet consectetur adipisicing elit. Provident voluptatibus deleniti, quae optio facere animi magnam possimus impedit soluta consectetur eum est commodi, eaque aperiam modi, minus repellat consequuntur officiis ad iusto. Unde quasi obcaecati quo quibusdam, nesciunt tempore, placeat itaque eaque alias ex in corrupti omnis consequatur expedita aut?</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-left">
                        <p class="m-0">Lorem, ipsum.</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="chat-date-wrapper">
                        <span class="date">2020-11-04</span>
                    </div>
                    <div class="speech-bubble-right">
                        <p class="m-0">Lorem, ipsum.</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-left">
                        <p class="m-0">That was a good chat ! See ya soon</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                    <div class="speech-bubble-right">
                        <p class="m-0">Bye !</p>
                        <p class="m-0 f-10 pl-4 text-right">12.02 p.m</p>
                    </div>
                </div>
                <div class="send-box">
                    <form action="#">    
                        <div class="send-wrapper">
                            <input type="text" placeholder="Send Message">
                            <button class="btn p-0">
                                <i class="fas fa-paper-plane mx-2"></i>
                            </button>
                        </div>
                    </form>
                </div>
                @else
                {{-- Default message when no user is selected --}}
                    <div class="default-message">
                        <h3>Select a user to start chatting</h3>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )

@endsection
