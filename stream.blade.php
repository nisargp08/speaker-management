@extends( 'virtual::public.layouts.app' )

@section( 'styles' )
<style>
.stream_page {
	 max-width: 1920px;
	 margin: 0 auto;
	/* Scrollbar width */
	/* Scrollbar Track */
	/* Scrollbar Handle */
	/* Scrollbar Handle on hover */
}
 .stream_page .f-10 {
	 font-size: 10px;
}
 .stream_page .f-14 {
	 font-size: 14px;
}
 .stream_page .font-weight-600 {
	 font-weight: 600;
}
 .stream_page .title {
	 font-weight: 600;
	 padding: 1.5rem 1.5rem 0.5rem 1.5rem;
}
 .stream_page .req_padding {
	 padding: 16px 16px 0 16px;
}
 .stream_page ::-webkit-scrollbar {
	 width: 5px;
}
 .stream_page ::-webkit-scrollbar-track {
	 background: #f1f1f1;
}
 .stream_page ::-webkit-scrollbar-thumb {
	 background: #888;
}
 .stream_page ::-webkit-scrollbar-thumb:hover {
	 background: #555;
}
 .stream_page .wrapper {
	 padding: 0 24px;
}
 .stream_page .wrapper .video_wrapper iframe {
	 width: 100%;
	 height: 720px;
}
 .stream_page .wrapper .video_stats {
	 display: flex;
	 align-items: center;
	 justify-content: flex-end;
}
 .stream_page .wrapper .video_stats > div {
	 display: flex;
	 align-items: center;
	 justify-content: center;
	 color: #f65855;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-header, .stream_page .wrapper .left-side .video_info_wrapper .session-body {
	 background-color: whitesmoke;
	 box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);
	 margin: 1rem 0;
	 padding: 2rem;
	 border-radius: 8px;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title {
	 display: flex;
	 justify-content: space-between;
	 flex-wrap: wrap;
	 align-items: center;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title .left-side .location {
	 font-size: 14px;
	 margin-bottom: 1rem;
	 color: #2a4365;
}
 @media (min-width: 768px) {
	 .stream_page .wrapper .left-side .video_info_wrapper .session-title .left-side .location {
		 margin-bottom: 0;
	}
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title .right-side {
	 margin-top: 20px;
	 display: flex;
	 flex-direction: column;
	 gap: 0.5rem;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title .right-side .bookmark-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #2d3748;
	 color: whitesmoke;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title .right-side .session-zoom {
	 margin-top: 1rem;
	 text-align: center;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-title .right-side .session-zoom .zoom-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 background-color: #4299e1;
	 color: whitesmoke;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-date-time {
	 display: flex;
	 align-items: center;
	 flex-wrap: wrap;
	 font-size: 14px;
	 color: #1a202c;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-date-time .remind-btn {
	 background-color: #cbd5e0;
	 border-radius: 8px;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-speakers-wrapper .title {
	 font-weight: 600;
	 font-size: 16px;
	 color: gray;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-speakers {
	 display: flex;
	 flex-wrap: wrap;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-speakers .speaker {
	 display: flex;
	 align-items: center;
	 padding: 5px 0;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-speakers .speaker img {
	 max-width: 100%;
	 height: auto;
	 border-radius: 50%;
	 width: 42px;
	 height: 42px;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-speakers .speaker .speaker-name {
	 margin: 0;
	 padding: 0 10px;
	 font-size: 14px;
	 color: #2c5282;
	 font-weight: 600;
}
 .stream_page .wrapper .left-side .video_info_wrapper .download-btn {
	 font-size: 14px;
	 border-radius: 8px;
	 border: 1px solid #2d3748;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-body h5 {
	 color: #2c5282;
}
 .stream_page .wrapper .left-side .video_info_wrapper .session-body ul {
	 margin-left: 2rem;
	 list-style-type: disc;
}
 .stream_page .wrapper .right-side .operations-wrapper {
	 display: flex;
	 align-items: center;
	 justify-content: flex-end;
}
 .stream_page .wrapper .right-side .chat_wrapper, .stream_page .wrapper .right-side .live_users_wrapper {
	 display: flex;
	 flex-direction: column;
	 background-color: whitesmoke;
	 width: 100%;
	 border-radius: 8px;
}
 .stream_page .wrapper .right-side .chat_wrapper .display-box, .stream_page .wrapper .right-side .live_users_wrapper .display-box {
	 flex: 1;
	 max-height: 610px;
	 overflow-y: auto;
	 padding: 16px;
	 border-top: 1px solid #e2e8f0;
}
 .stream_page .wrapper .right-side .chat_wrapper .display-box .user-list, .stream_page .wrapper .right-side .live_users_wrapper .display-box .user-list {
	 margin: 0.5rem 0;
}
 .stream_page .wrapper .right-side .chat_wrapper .display-box .user-list .avatar, .stream_page .wrapper .right-side .live_users_wrapper .display-box .user-list .avatar {
	 width: 32px;
}
 .stream_page .wrapper .right-side .chat_wrapper .display-box .user-list .avatar img, .stream_page .wrapper .right-side .live_users_wrapper .display-box .user-list .avatar img {
	 border-radius: 50%;
	 max-width: 100%;
	 height: auto;
}
 .stream_page .wrapper .right-side .chat_wrapper .display-box .user-list .info, .stream_page .wrapper .right-side .live_users_wrapper .display-box .user-list .info {
	 color: #12818e;
}
 .stream_page .wrapper .right-side .chat_wrapper .input-box, .stream_page .wrapper .right-side .live_users_wrapper .input-box {
	 background-color: whitesmoke;
	 padding: 10px 13px;
	 border-top: 1px solid #e2e8f0;
}
 .stream_page .wrapper .right-side .chat_wrapper .input-box .input-wrapper, .stream_page .wrapper .right-side .live_users_wrapper .input-box .input-wrapper {
	 background-color: white;
	 border-radius: 20px;
	 display: flex;
	 align-items: center;
	 justify-content: space-between;
	 padding: 10px;
}
 .stream_page .wrapper .right-side .chat_wrapper .input-box .input-wrapper input, .stream_page .wrapper .right-side .live_users_wrapper .input-box .input-wrapper input {
	 background-color: transparent;
	 border: none;
	 flex: 1;
}
 .stream_page .wrapper .right-side .chat_wrapper .input-box .input-wrapper input:focus, .stream_page .wrapper .right-side .live_users_wrapper .input-box .input-wrapper input:focus {
	 outline: none;
}
 .stream_page .wrapper .right-side .chat_wrapper .input-box .input-wrapper input::placeholder, .stream_page .wrapper .right-side .live_users_wrapper .input-box .input-wrapper input::placeholder {
	 color: #e3e3e3;
	 font-weight: 300;
	 margin-left: 20px;
	 font-size: 14px;
}
</style>
@endsection

@section( 'title' )
	Virtual Portal
@endsection

@section( 'header' )
	<h2>Event Title</h2>
@endsection

@section( 'content' )
    <div class="stream_page">
		{{-- Update name with current stream --}}
        <h5 class="title">Live Stream - Stream Name</h5>
		<hr>
		<div class="row wrapper">
			<div class="col-lg-8 left-side">
				<div class="video_wrapper">
					{{-- If using <video> to display the stream video then give height and width as given to iframe below --}}
					<iframe width="100%" height="720px" src="https://www.youtube.com/embed/pJvbOOP9ruA?list=RDpJvbOOP9ruA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<div class="video_stats">
						{{-- Live spectators --}}
						<div title="Live spectators">
							<i class="fas fa-users"></i>
							<p class="m-0 pl-2 py-2 font-weight-600">128</p>
						</div>
					</div>
				</div>
				<div class="video_info_wrapper">
					<div class="session-header">
						<div class="session-title">
							<div class="left-side">
								<h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
								<div class="location">	
									<i class="fas fa-map-marker-alt"></i>
									Plaza AB(2nd floor)
								</div>
								<div class="session-date-time">
									<span class="date">
										<i class="fas fa-calendar-week mr-1"></i>
										Wednesday, 29th April
									</span>
									<span class="time mx-2">
										<i class="fas fa-clock mr-1"></i>
										14:15 - 17:30
									</span>
									<button class="btn btn-sm remind-btn">
										<i class="fas fa-bell"></i> 
										Remind Me
									</button>
								</div>
							</div>
							<div class="right-side">
								<button class="btn bookmark-btn"><i class="far fa-bookmark mr-2"></i>Bookmark Session</button>
								<button class="btn download-btn"><i class="fas fa-download mr-2"></i>Download Resources</button>
								<div class="session-zoom">
									<p class="m-0 p-0 f-10">Webinar Zoom ID : 45678954</p>
									<p class="m-0 p-0 f-10">Passcode : 217502</p>
									<button class="btn zoom-btn"><i class="fas fa-video mr-2"></i>Join Zoom Webinar</button>
								</div>
							</div>
						</div>
						<hr>
						<div class="session-speakers-wrapper mt-8">
							<h5 class="title p-0">Speakers</h5>
							<div class="session-speakers">
								<div class="speaker">
									<img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="Speaker">   
									{{-- Hyperlink to speakers profile --}}
									<a href="#" target="_blank" class="speaker-name">Anthony Gray</a>
								</div>
								<div class="speaker">
									<img src="https://randomuser.me/api/portraits/women/63.jpg" alt="Speaker">      
									{{-- Hyperlink to speakers profile --}}
									<a href="#" target="_blank" class="speaker-name">Alia Pitts</a>
								</div>
								<div class="speaker">
									<img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="Speakers">     
									{{-- Hyperlink to speakers profile --}}
									<a href="#" target="_blank" class="speaker-name">Britney Cooper</a>
								</div>
							</div>
						</div>
					</div>
					<div class="session-body">
							<div class="session-description">
								<h5 class="font-weight-600">Description <span class="colon">:</span></h5>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae, cumque perferendis, minus non sapiente quaerat deleniti nam aut optio error reiciendis? Totam, cum accusantium modi magni suscipit libero eaque corporis mollitia veniam error illum alias vero quis temporibus! Sunt, aspernatur aperiam quisquam, praesentium, tempore incidunt expedita earum perspiciatis quod suscipit placeat mollitia ex fuga quis. Nisi voluptatum quam maiores magnam similique impedit, ratione earum dolorum aspernatur maxime rerum consectetur adipisci, unde repudiandae est tempore, ipsa consequuntur quae amet mollitia nulla laborum! Ex, explicabo! Accusantium soluta deserunt repudiandae illo dicta impedit, ratione consectetur numquam illum quidem sapiente magnam! Molestias et fuga debitis? Eius numquam at quas saepe error blanditiis tempore eos, illum velit et, ipsa cupiditate voluptate. Fuga ipsam alias excepturi odio quae nisi dolorem dolores illum numquam obcaecati, pariatur quis nostrum ea eius deleniti beatae odit optio maiores quo blanditiis dolorum ut harum dicta! Sequi corrupti fugiat dolor explicabo culpa molestias sunt id! Perspiciatis, eos nihil. Nobis reprehenderit similique iure, quisquam dignissimos at magni repellat? Sequi doloribus molestias harum magni vero enim, expedita tenetur distinctio quae placeat iste eos! Necessitatibus, ex molestiae. Libero autem perspiciatis facilis soluta ipsam veritatis sint numquam eos. Culpa beatae unde esse blanditiis molestiae assumenda accusamus.
							</div>
							<hr>
							<div class="session-objectives">
								<h5 class="font-weight-600">Learning Objectives <span class="colon">:</span></h5>
								<ul>
									<li>Lorem ipsum dolor sit amet.</li>
									<li>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Assumenda fugit nostrum sapiente qui! Quod quaerat voluptates dolor placeat magni officiis!</li>
									<li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis, quis.</li>
								</ul>
							</div>
							<hr>
							<div class="session-evaluation">
								<h5 class="font-weight-600">Evaluation <span class="colon">:</span></h5>
								{{-- Session Evaluation Link --}}
								<p><a href="#">Click here</a> to evaluate this session</p>
							</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 right-side">
				{{-- Chat/User Switcher box --}}
				<div class="operations-wrapper">
					{{-- View chat button --}}
					<button id="live-chat" class="btn" title="Users chat">
						<i class="fas fa-comments"></i>
					</button>
					{{-- View users spectating button --}}
					<button id="live-users" class="btn" title="Users spectating">
						<i class="fas fa-users"></i>
					</button>
				</div>
				{{-- Chat message box --}}
				<div class="chat_wrapper">
					<h5 class="font-weight-600 req_padding">Chat</h5>
					<div class="display-box">
						{{-- Looping for demo purposes --}}
						@for ($i = 0; $i < 2; $i++)
						{{-- Looping for demo purposes --}}
							@for ($i = 0; $i < 5; $i++)
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Anthony</span>
										<span class="lname f-14 font-weight-600">Gray</span>
									</div>
									{{-- User chat message --}}
									<span class="colon font-weight-600 f-14">:</span>
									<span class="message d-inline f-14">Lorem, ipsum.</span>
								</div>
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://randomuser.me/api/portraits/women/63.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Alia</span>
										<span class="lname f-14 font-weight-600">Pitts</span>
									</div>
									{{-- User chat message --}}
									<span class="colon font-weight-600 f-14">:</span>
									<span class="message d-inline f-14">Lorem ipsum dolor sit amet.</span>
								</div>  
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://randomuser.me/api/portraits/men/86.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Brad</span>
										<span class="lname f-14 font-weight-600">Newman</span>
									</div>
									{{-- User chat message --}}
									<span class="colon font-weight-600 f-14">:</span>
									<span class="message d-inline f-14">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Incidunt, voluptas.</span>
								</div>  
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Britney</span>
										<span class="lname f-14 font-weight-600">Cooper</span>
									</div>
									{{-- User chat message --}}
									<span class="colon font-weight-600 f-14">:</span>
									<span class="message d-inline f-14">Lorem ipsum dolor sit amet consectetur adipisicing elit.</span>
								</div>  
							@endfor
						@endfor
					</div>
					<div class="input-box">
						<form action="#">    
							<div class="input-wrapper">
								<input type="text" placeholder="Send Message">
								<button class="btn p-0">
									<i class="fas fa-paper-plane mx-2"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
				{{-- All the live users box : Hidden by default --}}
				<div class="live_users_wrapper">
					<h5 class="font-weight-600 req_padding">Users in chat</h5>
					<div class="input-box">
						<form action="#">    
							<div class="input-wrapper">
								<input type="text" placeholder="Search User">
								<button class="btn p-0">
									<i class="fas fa-paper-plane mx-2"></i>
								</button>
							</div>
						</form>
					</div>
					<div class="display-box">
						{{-- Looping for demo purposes --}}
						@for ($i = 0; $i < 2; $i++)
						{{-- Looping for demo purposes --}}
							@for ($i = 0; $i < 5; $i++)
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://uifaces.co/our-content/donated/gPZwCbdS.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Anthony</span>
										<span class="lname f-14 font-weight-600">Gray</span>
									</div>
								</div>
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://randomuser.me/api/portraits/women/63.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Alia</span>
										<span class="lname f-14 font-weight-600">Pitts</span>
									</div>
								</div>  
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://randomuser.me/api/portraits/men/86.jpg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Brad</span>
										<span class="lname f-14 font-weight-600">Newman</span>
									</div>
								</div>  
								<div class="user-list">
									<div class="avatar d-inline-block">
										{{-- Avatar --}}
										<img src="https://uifaces.co/our-content/donated/3799Ffxy.jpeg" alt="User">        
									</div>
									<div class="info d-inline-block">
										{{-- First,last name --}}
										<span class="fname f-14 font-weight-600">Britney</span>
										<span class="lname f-14 font-weight-600">Cooper</span>
									</div>
								</div>  
							@endfor
						@endfor
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	{{-- Schedule Session flex card --}}
	<div class="session-schedule-card">
		<h4 class="font-weight-600">A1: Using Dashboards to Visualize Data & Gain Insights</h4>
		<div class="location">	<i class="fas fa-map-marker-alt mr-2"></i>Plaza AB(2nd floor)</div>
		<div class="session-date-time">
			<span class="date"><i class="fas fa-calendar-week mr-1"></i>Wednesday, 29th April</span>
			<span class="time mx-2"><i class="fas fa-clock mr-1"></i>14:15 - 17:30</span>
			<button class="btn btn-sm remind-btn"><i class="fas fa-bell"></i> Remind Me</button>
		</div>
	</div>
@endsection

@section( 'footer' )
	@include( 'virtual::public.includes.footer' )
@endsection

@section( 'js' )
	<script>
		//Hiding live users by default
		$('.live_users_wrapper').hide();
		//Switching panels based on click
		$('#live-chat').click(function(){
			//Hide users
			$('.live_users_wrapper').hide();
			//Show Chat
			$('.chat_wrapper').css('display','flex');
		});
		$('#live-users').click(function(){
			//Hide chat
			$('.chat_wrapper').hide();
			//Show Users
			$('.live_users_wrapper').css('display','flex');
		});
	</script>
@endsection
